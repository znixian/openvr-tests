#pragma once
#include <map>
#include <string>
#include <vector>

/**
 * Simple and limited parser for SudoFont fonts. SudoFont is a small format for storing
 * font metadata, and includes a programme to convert fonts into it's format.
 *
 * This loader code is from OpenComposite, and not based on SudoFont itself.
 *
 * See https://github.com/SudoMike/SudoFont.
 */
class SudoFontMeta {
  public:
	SudoFontMeta(std::vector<uint8_t> data);
	~SudoFontMeta();

	struct CharInfo {
		// Which character this Character references.
		wchar_t CharacterCode;

		// Location of this character in the packed image.
		short PackedX;
		short PackedY;
		short PackedWidth;
		short PackedHeight;

		// Where to draw this character on the target.
		short XOffset;
		short YOffset;

		// How much to advance your X position after drawing this character.
		// The total amount to advance for each char is ( XAdvance + GetKerning( nextChar ) ).
		short XAdvance;
	};

	int Width(wchar_t chr);
	int Width(std::wstring str);

	int GetImageWidth() { return m_imgWidth; }
	int GetImageHeight() { return m_imgHeight; }
	int GetLineHeight() { return m_lineHeight; }

	std::optional<SudoFontMeta::CharInfo> GetCharInfo(wchar_t chr) const;

  private:
	std::map<wchar_t, CharInfo> m_chars;

	int m_imgWidth = -1;
	int m_imgHeight = -1;

	int m_lineHeight = -1;
};
