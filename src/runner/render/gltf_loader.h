//
// Created by znix on 23/06/2021.
//

#pragma once

#include "Model.h"

#include <string>

class IGltfModel : public IModel {
	// Only the public methods of the GLTF model are declared here as there's a lot of private variables in use
  public:
	virtual void SetBone(std::string name, glm::mat4 transform) = 0;
	virtual glm::mat4 GetBoneBindPose(std::string name) = 0;
};

int load_gltf(IModel **out, std::string filename, std::string textureFilename);
