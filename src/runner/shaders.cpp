//
// Created by ZNix on 13/06/2021.
//

#include "shaders.h"

const char *get_vert_shader_src() {
	return R"(#version 420 core
layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 in_norm;
layout (location = 2) in ivec4 joints;
layout (location = 3) in vec4 weights;
layout (location = 4) in vec2 in_uv;

out vec3 norm;
out vec2 uv;

layout (std140, binding = 0) uniform BONE_BINDING {
	int bone_count;
	int padding2;
	int padding3;
	int padding4;
	mat4 inverse_bind[64];
	mat4 current_pose[64];
} bones;

uniform mat4 obj_to_cam;

void main() {
	norm = in_norm;
	uv = in_uv;

	// Build the skin deform matrix by blending the poses by weight
	// Yes, it'd be more efficient to premultiply the inverse bind poses into the poses in
	// the rendering code. No, I don't care, it's perfectly fine here.
	mat4 deformation =
		((bones.current_pose[joints.x] * bones.inverse_bind[joints.x]) * weights.x) +
		((bones.current_pose[joints.y] * bones.inverse_bind[joints.y]) * weights.y) +
		((bones.current_pose[joints.z] * bones.inverse_bind[joints.z]) * weights.z) +
		((bones.current_pose[joints.w] * bones.inverse_bind[joints.w]) * weights.w)
	;

	vec4 skinned_pos = vec4(pos, 1.0f);
	if (bones.bone_count != 0) {
		skinned_pos = deformation * skinned_pos;
	} else {
		// No bones, no bone transform required
	}

	skinned_pos.w = 1; // Sometimes breaks?
	gl_Position = obj_to_cam * skinned_pos;
}
)";
}
const char *get_frag_shader_src() {
	return R"(#version 420 core
out vec4 FragColour;
in vec3 norm;
in vec2 uv;

uniform vec3 light_dir = normalize(vec3(0.25, -0.5, -0.15));
uniform mat4 obj_to_world;

uniform sampler2D tex;

void main() {
    // Find the 'effective' normal, in world space. This makes
    // the lighting take account of the hand's rotation.
    // Put zero in the last column to avoid the matrix translating it.
    vec3 effective_normal = (obj_to_world * vec4(norm, 0.0f)).xyz;

    float diffuse_int = dot(effective_normal, light_dir);
	diffuse_int = max(diffuse_int, 0.0f);

	vec3 base_colour = texture(tex, uv).rgb; // vec3(0.5f, 0.5f, 0.0f);
	vec3 colour = base_colour * (diffuse_int + 0.5);
	FragColour = vec4(colour, 1.0f);
})";
}

const char *get_line_vert_shader_src() {
	return R"(#version 410 core
layout (location = 0) in vec4 pos;

uniform mat4 obj_to_cam_line;

void main() {
    gl_Position = obj_to_cam_line * pos;
}
)";
}
const char *get_line_frag_shader_src() {
	return R"(#version 410 core
out vec4 FragColour;

void main() {
    FragColour = vec4(1.0f, 1.0f, 1.0f, 1.0f);
})";
}

const char *get_font_vert_shader_src() {
	return R"(#version 420 core
out vec2 uv;

uniform mat4 obj_to_cam;
uniform mat2 uv_spec; // This is a misused matrix, first column is offset and second is size

void main() {
	vec2 pos;
	if (gl_VertexID == 0) {
		pos = vec2(0, 0);
	} else if (gl_VertexID == 1 || gl_VertexID == 3) {
		pos = vec2(1, 0);
	} else if (gl_VertexID == 2 || gl_VertexID == 4) {
		pos = vec2(0, 1);
	} else if (gl_VertexID == 5) {
		pos = vec2(1, 1);
	}

	// Note the image is flipped but the character positions work with those flipped values, so we
	// need to just flip what's drawn.
	vec2 uvPos = vec2(pos.x, 1-pos.y);
	uv = uv_spec[0] + uvPos*uv_spec[1];

	// Use the size of the UV to find out how large an area to draw
	vec2 size = uv_spec[1];
	gl_Position = obj_to_cam * vec4(pos*size, 0f, 1.0f);
}
)";
}
const char *get_font_frag_shader_src() {
	return R"(#version 420 core
out vec4 FragColour;
in vec2 uv;

uniform sampler2D tex;

void main() {
	// Only the alpha channel is important
	float a = texture(tex, uv).a;
	if (a < 0.5)
		discard;
	FragColour = vec4(1.0f, 1.0f, 1.0f, 1.0f);
})";
}
