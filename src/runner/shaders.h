//
// Created by ZNix on 13/06/2021.
//

#pragma once

const char *get_frag_shader_src();
const char *get_vert_shader_src();

const char *get_line_frag_shader_src();
const char *get_line_vert_shader_src();

const char *get_font_vert_shader_src();
const char *get_font_frag_shader_src();
