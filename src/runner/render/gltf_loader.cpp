//
// Created by znix on 06/09/22.
//

#include "global.h"

#include "gltf_loader.h"

#include "stb_image.h"
#include "tiny_gltf.h"

#include <span>
#include <stdexcept>

template <typename T, size_t Extent> T &span_at(std::span<T, Extent> span, size_t idx) {
	if (idx >= span.size()) {
		throw std::out_of_range("Bad span index");
	}
	return span[idx];
}

class GltfModel : public IGltfModel {
  public:
	VAO GetVAO() override { return vao; }
	void Draw(GLuint shader) override {
		// Update and bind the UBO
		glBindBufferBase(GL_UNIFORM_BUFFER, 0, ubo); // 0 is set as the binding in the shader
		glBufferSubData(GL_UNIFORM_BUFFER, 0, uboData.size(), uboData.data());

		glBindTexture(GL_TEXTURE_2D, texture);

		// Draw the object
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
		// Double-cast to avoid clion warning
		glDrawElements(GL_TRIANGLES, vertices, verticesType, (void *)(ptrdiff_t)verticesByteOffset);
	}

	void SetBone(std::string name, glm::mat4 transform) override {
		int offset = boneIndexesByName.at(name);
		memcpy(uboData.data() + offset, glm::value_ptr(transform), 64);
	}

	glm::mat4 GetBoneBindPose(std::string name) override { return boneBindPoses.at(name); }

	VAO vao = -1;
	VBO ebo = -1;
	VBO ubo = -1;
	int vertices = 0;
	int verticesByteOffset = 0;
	GLenum verticesType = 0;
	std::vector<uint8_t> uboData;
	std::map<std::string, int> boneIndexesByName;
	std::map<std::string, glm::mat4> boneBindPoses;
	Texture texture = 0;
};

class DataAccessor {
  public:
	const tinygltf::Accessor *accessor = nullptr;
	const tinygltf::BufferView *view = nullptr;
	const tinygltf::Buffer *buffer = nullptr;
	int stride = -1;
	std::span<uint8_t> data;
};

class GltfTreeNode {
  public:
	std::string name;
	int id = -1;
	glm::mat4 localTransform = glm::identity<glm::mat4>();
	GltfTreeNode *parent = nullptr;
	int mesh = -1;
	int skin = -1;

	glm::mat4 GetWorldTransform() {
		if (parent == nullptr)
			return localTransform;
		return parent->GetWorldTransform() * localTransform;
	}
};

DataAccessor lookup_accessor(tinygltf::Model &model, int accessorId) {
	const tinygltf::Accessor &accessor = model.accessors.at(accessorId);
	const tinygltf::BufferView &view = model.bufferViews.at(accessor.bufferView);
	tinygltf::Buffer &buffer = model.buffers.at(view.buffer); // Non-const so we can access buffer.data.data

	int stride = accessor.ByteStride(view);

	int start = view.byteOffset + accessor.byteOffset;
	int length = accessor.count * stride;
	assert(length <= view.byteLength - accessor.byteOffset);

	std::span<uint8_t> data(buffer.data.data() + start, length);

	return DataAccessor{
	    .accessor = &accessor,
	    .view = &view,
	    .buffer = &buffer,
	    .stride = stride,
	    .data = data,
	};
}

GLenum gltf_to_gl_component_type(int type) {
	switch (type) {
	case TINYGLTF_COMPONENT_TYPE_BYTE:
		return GL_BYTE;
	case TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE:
		return GL_UNSIGNED_BYTE;
	case TINYGLTF_COMPONENT_TYPE_SHORT:
		return GL_SHORT;
	case TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT:
		return GL_UNSIGNED_SHORT;
	case TINYGLTF_COMPONENT_TYPE_INT:
		return GL_INT;
	case TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT:
		return GL_UNSIGNED_INT;
	case TINYGLTF_COMPONENT_TYPE_FLOAT:
		return GL_FLOAT;
	case TINYGLTF_COMPONENT_TYPE_DOUBLE:
		return GL_DOUBLE;
	default:
		fprintf(stderr, "Unknown GLTF component type %d\n", type);
		abort();
	}
}

int load_gltf(IModel **out, std::string filename, std::string textureFilename) {
	*out = nullptr;

	tinygltf::Model model;
	tinygltf::TinyGLTF gltfCtx;
	std::string err, warn;
	bool success = gltfCtx.LoadBinaryFromFile(&model, &err, &warn, filename);

	if (!success || !err.empty()) {
		fprintf(stderr, "Failed to load GLTF asset %s: %s\n", filename.c_str(), err.c_str());
		return 1;
	}

	if (!warn.empty()) {
		fprintf(stderr, "Loaded GLTF asset %s with warning: %s\n", filename.c_str(), warn.c_str());
	}

	// Build a tree of all the nodes
	std::vector<GltfTreeNode> nodes;
	GltfTreeNode *meshNode = nullptr;
	int numMeshNodes = 0;
	nodes.resize(model.nodes.size());
	for (int i = 0; i < model.nodes.size(); i++) {
		GltfTreeNode &outNode = nodes.at(i);
		const tinygltf::Node &inNode = model.nodes.at(i);

		outNode.id = i;
		outNode.name = inNode.name;

		if (!inNode.translation.empty()) {
			for (int axis = 0; axis < 3; axis++)
				outNode.localTransform[3][axis] = inNode.translation.at(axis);
		}

		if (!inNode.rotation.empty()) {
			glm::quat quaternion; // GLM uses WXYZ but glTF uses XYZW
			for (int xyz = 0; xyz < 3; xyz++)
				quaternion[xyz + 1] = inNode.rotation.at(xyz);
			quaternion[0] = inNode.rotation.at(3); // w

			// When applying the rotation, order is important: first apply rotation, then translation
			outNode.localTransform *= glm::mat4_cast(quaternion);
		}

		for (int childId : inNode.children) {
			GltfTreeNode &child = nodes.at(childId);
			assert(child.parent == nullptr);
			child.parent = &outNode;
		}

		if (inNode.mesh != -1) {
			meshNode = &outNode;
			outNode.mesh = inNode.mesh;
			outNode.skin = inNode.skin;
			numMeshNodes++;
		}
	}

	if (numMeshNodes != 1) {
		fprintf(stderr, "GLTF asset %s had non-one number of mesh nodes: %d\n", filename.c_str(), numMeshNodes);
		return 1;
	}

	tinygltf::Mesh &mesh = model.meshes.at(meshNode->mesh);
	if (mesh.primitives.size() != 1) {
		fprintf(stderr, "GLTF asset %s had non-one number of primitives: %d\n", filename.c_str(),
		        (int)mesh.primitives.size());
		return 1;
	}
	tinygltf::Primitive &prim = mesh.primitives.at(0);

	if (prim.indices == -1) {
		fprintf(stderr, "GLTF asset %s had non-indexed primitive.\n", filename.c_str());
		return 1;
	}
	DataAccessor indices = lookup_accessor(model, prim.indices);
	assert(indices.accessor->type == TINYGLTF_TYPE_SCALAR);

	// Build the VAO with all the vertex attributes
	// Note we can just directly map the buffer view into a VBO

	VAO vao = 0;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// TODO glBindBuffer(GL_ARRAY_BUFFER, triangle);

	std::map<const tinygltf::BufferView *, VBO> vbos;

	for (const auto &[name, accessorId] : prim.attributes) {
		// Find the attribute to write into to match up with the shader
		int attribId;
		if (name == "POSITION") {
			attribId = 0;
		} else if (name == "NORMAL") {
			attribId = 1;
		} else if (name == "JOINTS_0") {
			attribId = 2;
		} else if (name == "WEIGHTS_0") {
			attribId = 3;
		} else if (name == "TEXCOORD_0") {
			attribId = 4;
		} else {
			continue;
		}

		DataAccessor accessor = lookup_accessor(model, accessorId);

		// If we've already created a VBO for the buffer view then use that, otherwise make a new one
		if (vbos.contains(accessor.view)) {
			VBO vbo = vbos.at(accessor.view);
			glBindBuffer(GL_ARRAY_BUFFER, vbo);
		} else {
			VBO vbo = 0;
			const uint8_t *data = accessor.buffer->data.data() + accessor.view->byteOffset;
			glGenBuffers(1, &vbo);
			glBindBuffer(GL_ARRAY_BUFFER, vbo);
			glBufferData(GL_ARRAY_BUFFER, accessor.view->byteLength, data, GL_STATIC_DRAW);
			vbos[accessor.view] = vbo;
		}

		int elementsPerVert = tinygltf::GetNumComponentsInType(accessor.accessor->type);

		GLenum componentType = gltf_to_gl_component_type(accessor.accessor->componentType);

		if (componentType == GL_FLOAT || componentType == GL_DOUBLE) {
			glVertexAttribPointer(attribId, elementsPerVert, componentType, GL_FALSE, accessor.stride,
			                      (void *)accessor.accessor->byteOffset);
		} else {
			glVertexAttribIPointer(attribId, elementsPerVert, componentType, accessor.stride,
			                       (void *)accessor.accessor->byteOffset);
		}
		glEnableVertexAttribArray(attribId);
	}

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	// Produce the EBO
	GLenum componentType = gltf_to_gl_component_type(indices.accessor->componentType);
	GLuint ebo = 0;
	glGenBuffers(1, &ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.view->byteLength,
	             indices.buffer->data.data() + indices.view->byteOffset, GL_STATIC_DRAW);

	// Build a list of all the nodes that correspond to the joints in the skinned model
	std::vector<GltfTreeNode *> joints;
	tinygltf::Skin *skin = nullptr;
	if (meshNode->skin != -1) {
		skin = &model.skins.at(meshNode->skin);

		joints.resize(skin->joints.size());
		for (int i = 0; i < skin->joints.size(); i++) {
			joints.at(i) = &nodes.at(skin->joints.at(i));
		}
	}

	// Make the data area for the UBO, which will be modified and attached at draw time
	std::vector<uint8_t> uboData;
	uboData.resize(16); // Initial area containing the number of vertices, padded upto vce4 size as per std140
	uboData.at(0) = joints.size();

	// Load the inverse bind matrices
	if (skin != nullptr) {
		DataAccessor inverseBind = lookup_accessor(model, skin->inverseBindMatrices);
		int bindByteSize = 64 * skin->joints.size();
		uboData.insert(uboData.end(), inverseBind.data.begin(), inverseBind.data.begin() + bindByteSize);
	}
	uboData.resize(16 + 64 * 64); // Resize it upto the full 64 matrices that are reserved for it

	// Reserve space for the actual poses
	int bindOffset = uboData.size();
	uboData.resize(uboData.size() + 64 * 64);

	// Calculate the default joint poses to use until finger tracking data is available
	std::map<std::string, glm::mat4> boneBindPoses;
	for (int i = 0; i < joints.size(); i++) {
		glm::mat4 transform = joints.at(i)->GetWorldTransform();
		memcpy(uboData.data() + bindOffset + 64 * i, glm::value_ptr(transform), 64);
		boneBindPoses[joints.at(i)->name] = transform;
	}

	// Produce a mapping from the bone names to the indexes into the UBO data
	std::map<std::string, int> boneIndexesByName;
	for (int i = 0; i < joints.size(); i++) {
		boneIndexesByName[joints.at(i)->name] = bindOffset + 64 * i;
	}

	// Create a UBO for it, this will be updated on each frame and contains both the inverse bind matrix and
	// the current poses.
	VBO ubo = -1;
	glGenBuffers(1, &ubo);
	glBindBuffer(GL_UNIFORM_BUFFER, ubo);
	glBufferData(GL_UNIFORM_BUFFER, uboData.size(), uboData.data(), GL_DYNAMIC_DRAW);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	// Load the texture
	int width, height;
	uint8_t *data = stbi_load(textureFilename.c_str(), &width, &height, nullptr, 4);
	if (data == nullptr) {
		const char *reason = stbi_failure_reason();
		fprintf(stderr, "Failed to load texture for GLTF: %s: %s\n", textureFilename.c_str(), reason);
		return 1;
	}

	Texture tex = 0;
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	glGenerateMipmap(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);

	GltfModel *outModel = new GltfModel;
	outModel->vao = vao;
	outModel->ebo = ebo;
	outModel->ubo = ubo;
	outModel->vertices = indices.accessor->count;
	outModel->verticesByteOffset = indices.accessor->byteOffset;
	outModel->verticesType = componentType;
	outModel->uboData = std::move(uboData);
	outModel->boneIndexesByName = std::move(boneIndexesByName);
	outModel->boneBindPoses = std::move(boneBindPoses);
	outModel->texture = tex;
	*out = outModel;
	return 0;
}
