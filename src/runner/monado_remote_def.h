// Copied from monado remote driver r_interface.h
// Copyright 2020, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0

#pragma once

#include <stdint.h>

/*!
 * A quaternion with single floats.
 *
 * @ingroup xrt_iface math
 */
// NOLINTNEXTLINE(readability-identifier-naming)
struct xrt_quat {
	float x;
	float y;
	float z;
	float w;
};

/*!
 * A 1 element vector with single floats.
 *
 * @ingroup xrt_iface math
 */
// NOLINTNEXTLINE(readability-identifier-naming)
struct xrt_vec1 {
	float x;
};

/*!
 * A 2 element vector with single floats.
 *
 * @ingroup xrt_iface math
 */
// NOLINTNEXTLINE(readability-identifier-naming)
struct xrt_vec2 {
	float x;
	float y;
};

/*!
 * A 3 element vector with single floats.
 *
 * @ingroup xrt_iface math
 */
// NOLINTNEXTLINE(readability-identifier-naming)
struct xrt_vec3 {
	float x;
	float y;
	float z;
};

/*!
 * A pose composed of a position and orientation.
 *
 * @see xrt_qaut
 * @see xrt_vec3
 * @ingroup xrt_iface math
 */
// NOLINTNEXTLINE(readability-identifier-naming)
struct xrt_pose {
	struct xrt_quat orientation;
	struct xrt_vec3 position;
};

/*!
 * Header value to be set in the packet.
 *
 * @ingroup drv_remote
 */
#define R_HEADER_VALUE (*(uint64_t *)"mndrmt1\0")

/*!
 * Data per controller.
 */
// NOLINTNEXTLINE(readability-identifier-naming)
struct r_remote_controller_data {
	struct xrt_pose pose;
	struct xrt_vec3 linear_velocity;
	struct xrt_vec3 angular_velocity;

	float hand_curl[5];

	struct xrt_vec1 trigger_value;
	struct xrt_vec1 squeeze_value;
	struct xrt_vec1 squeeze_force;
	struct xrt_vec2 thumbstick;
	struct xrt_vec1 trackpad_force;
	struct xrt_vec2 trackpad;

	bool hand_tracking_active;
	bool active;

	bool system_click;
	bool system_touch;
	bool a_click;
	bool a_touch;
	bool b_click;
	bool b_touch;
	bool trigger_click;
	bool trigger_touch;
	bool thumbstick_click;
	bool thumbstick_touch;
	bool trackpad_touch;
	bool _pad0;
	bool _pad1;
	bool _pad2;
	// active(2) + bools(11) + pad(3) = 16
};

/*!
 * Remote data sent from the debugger to the hub.
 *
 * @ingroup drv_remote
 */
// NOLINTNEXTLINE(readability-identifier-naming)
struct r_remote_data {
	uint64_t header;

	struct {
		struct xrt_pose pose;
	} hmd;

	struct r_remote_controller_data left, right;
};
