#pragma once
#include "tests.h"

#include "gen_TestInput.gen.h"

class TestInput {
	GeneratedInputClassBody;

  private:
	vr::VRActionSetHandle_t m_mainSet = vr::k_ulInvalidActionSetHandle;
	vr::VRActionSetHandle_t m_singleSidedSet = vr::k_ulInvalidActionSetHandle;

	vr::VRInputValueHandle_t m_leftHandSource = vr::k_ulInvalidInputValueHandle;
	vr::VRInputValueHandle_t m_rightHandSource = vr::k_ulInvalidInputValueHandle;

	vr::VRActionHandle_t m_btn1 = vr::k_ulInvalidActionSetHandle;
	vr::VRActionHandle_t m_dualHandPose = vr::k_ulInvalidActionSetHandle;
	vr::VRActionHandle_t m_skel = vr::k_ulInvalidActionSetHandle;

	bool m_firstDigitalUpdate = true;
};
