//
// Created by znix on 04/07/2021.
//

#include "vrtime.h"

#include <time.h>

#ifdef _WIN32

#include <Windows.h>

uint64_t get_current_time() {
	LARGE_INTEGER perfValue = {}, perfFreq = {};
	QueryPerformanceCounter(&perfValue);
	QueryPerformanceFrequency(&perfFreq);

	double nsPerCount = 1000000000.0 / perfFreq.QuadPart;
	return (uint64_t)(nsPerCount * perfValue.QuadPart);
}

#else

uint64_t get_current_time() {
	struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	return ts.tv_nsec + ts.tv_sec * 1000000000L;
}

#endif
