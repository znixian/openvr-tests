//
// Created by znix on 23/06/2021.
//

#include "global.h"

#include "obj_loader.h"

#include <fstream>
#include <functional>

static std::vector<std::string> split_string(const std::string &input, std::function<bool(char)> isSplitter) {
	bool lastSplitter = true;
	std::vector<std::string> parts;

	for (char c : input) {
		bool ws = isSplitter(c);
		if (ws) {
			lastSplitter = true;
			continue;
		}

		if (lastSplitter) {
			parts.push_back("");
			lastSplitter = false;
		}

		parts.back().push_back(c);
	}

	return parts;
}

static std::vector<std::string> split_whitespace(const std::string &input) { return split_string(input, isspace); }

static float chk_parse_int(const std::string &str) {
	const char *endptr = nullptr;
	long result = strtol(str.c_str(), (char **)&endptr, 10);

	if (*endptr) {
		fprintf(stderr, "Got invalid char in int '%s' - '%c'\n", str.c_str(), *endptr);
		abort();
	}

	return result;
}

static float chk_parse_float(const std::string &str) {
	const char *endptr = nullptr;
	float result = strtof(str.c_str(), (char **)&endptr);

	if (*endptr) {
		fprintf(stderr, "Got invalid char in float '%s' - '%c'\n", str.c_str(), *endptr);
		abort();
	}

	return result;
}

struct FacePoint {
	int pos;
	int uv;
	int norm;
};

static FacePoint chk_parse_face_point(const std::string &str) {
	std::vector<std::string> parts = split_string(str, [](char c) { return c == '/'; });
	assert(parts.size() == 3);

	FacePoint fp;
	fp.pos = chk_parse_int(parts.at(0));
	fp.uv = chk_parse_int(parts.at(1));
	fp.norm = chk_parse_int(parts.at(2));

	return fp;
}

int load_obj(IModel **out, std::string filename) {
	*out = nullptr;

	std::ifstream input;
	input.exceptions(std::ios::badbit);
	input.open(filename);

	struct RawTri {
		FacePoint a, b, c;
	};

	std::vector<glm::vec3> positions;
	std::vector<glm::vec3> normals;
	std::vector<RawTri> faces;

	std::string line;
	while (std::getline(input, line)) {
		if (line.empty())
			continue;
		if (line[0] == '#')
			continue;

		const std::vector<std::string> parts = split_whitespace(line);
		const std::string &cmd = parts[0];

		if (cmd == "v") {
			assert(parts.size() == 4);
			float x = chk_parse_float(parts.at(1));
			float y = chk_parse_float(parts.at(2));
			float z = chk_parse_float(parts.at(3));
			positions.push_back(glm::vec3(x, y, z));
		} else if (cmd == "vt") {
			// Do nothing for now
			// printf("Vert tex: %zu\n", parts.size());
		} else if (cmd == "vn") {
			assert(parts.size() == 4);
			float x = chk_parse_float(parts.at(1));
			float y = chk_parse_float(parts.at(2));
			float z = chk_parse_float(parts.at(3));
			normals.push_back(glm::vec3(x, y, z));
		} else if (cmd == "f") {
			assert(parts.size() == 4);
			FacePoint a = chk_parse_face_point(parts.at(1));
			FacePoint b = chk_parse_face_point(parts.at(2));
			FacePoint c = chk_parse_face_point(parts.at(3));
			faces.push_back(RawTri{a, b, c});
		} else if (cmd == "mtllib" || cmd == "o" || cmd == "usemtl" || cmd == "s") {
			// Ignore these
		} else {
			fprintf(stderr, "Invalid OBJ file arg %s\n", cmd.c_str());
			abort();
		}
	}

	Model *objPtr = new Model;
	*out = objPtr;
	Model &obj = *objPtr;

	obj.verts.resize(faces.size() * 3);
	obj.tris.resize(faces.size());
	for (int i = 0; i < faces.size(); i++) {
		RawTri &tri = faces.at(i);
		int vid = i * 3;

		auto buildVert = [&](const FacePoint &fp) {
			ModelVert vert{};
			// Note: -1 since obj is one-indexed
			vert.pos = positions.at(fp.pos - 1);
			vert.norm = normals.at(fp.norm - 1);
			return vert;
		};

		obj.verts.at(vid + 0) = buildVert(tri.a);
		obj.verts.at(vid + 1) = buildVert(tri.b);
		obj.verts.at(vid + 2) = buildVert(tri.c);

		obj.tris.at(i) = ModelTri{vid, vid + 1, vid + 2};
	}

	return 0;
}
