#include "global.h"

#include "SudoFontMeta.h"

#include <assert.h>

std::vector<uint8_t> readBytes(std::vector<uint8_t> &data, size_t len) {
	assert(len <= data.size());
	std::vector<uint8_t> result(data.begin(), data.begin() + len);
	data.erase(data.begin(), data.begin() + len);
	return result;
}

template <typename T> T readData(std::vector<uint8_t> &data) {
	T val;
	std::vector<uint8_t> sVal = readBytes(data, sizeof(val));
	memcpy(&val, sVal.data(), sizeof(val));

	return val;
}

enum SectionID_t : uint16_t {
	ID_FONT_INFO = 0,
	ID_CHARACTERS,
	ID_KERNING,
	ID_CONFIG,

	ID_END = 999,
};

SudoFontMeta::SudoFontMeta(std::vector<uint8_t> data) {
	std::string headerReq = "\x0bSudoFont1.1";
	std::vector<uint8_t> headerVec = readBytes(data, headerReq.length());
	std::string header((const char *)headerVec.data(), headerVec.size());
	assert(header == headerReq);

	unsigned int origTexW, origTexH;

	while (true) {
		auto sectionID = readData<SectionID_t>(data);

		if (sectionID == ID_END)
			break;

		auto sectionSize = readData<uint32_t>(data);

		std::vector<uint8_t> sec = readBytes(data, sectionSize);

		if (sectionID == ID_FONT_INFO) {
			m_lineHeight = readData<uint16_t>(sec);
			m_imgWidth = readData<uint16_t>(sec);
			m_imgHeight = readData<uint16_t>(sec);
		} else if (sectionID == ID_CHARACTERS) {
			auto count = readData<uint16_t>(sec);

			for (size_t i = 0; i < count; i++) {
				CharInfo info = {0};

				info.CharacterCode = readData<uint16_t>(sec);

				info.PackedX = readData<uint16_t>(sec);
				info.PackedY = readData<uint16_t>(sec);
				info.PackedWidth = readData<uint16_t>(sec);
				info.PackedHeight = readData<uint16_t>(sec);

				info.XOffset = readData<uint16_t>(sec);
				info.YOffset = readData<uint16_t>(sec);

				info.XAdvance = readData<uint16_t>(sec);

				assert(!m_chars.contains(info.CharacterCode));
				m_chars[info.CharacterCode] = info;
			}
		}
	}
}

SudoFontMeta::~SudoFontMeta() {}

int SudoFontMeta::Width(wchar_t chr) {
	const CharInfo &info = m_chars.at(chr);
	return info.XAdvance;
}

int SudoFontMeta::Width(std::wstring str) {
	int width = 0;
	for (wchar_t chr : str)
		width += Width(chr);
	return width;
}

std::optional<SudoFontMeta::CharInfo> SudoFontMeta::GetCharInfo(wchar_t chr) const {
	if (!m_chars.contains(chr))
		return {}; // empty optional
	return m_chars.at(chr);
}
