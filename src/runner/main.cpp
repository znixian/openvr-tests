#include "global.h"

#include "asserts.h"
#include "render.h"

#include "interfaces/TestInput.h"
#include "interfaces/TestSystem.h"
#include "monado_remote.h"
#include "steamvr_dbg.h"

#include <stdio.h>

#include <GLFW/glfw3.h>
#include <glm/gtx/quaternion.hpp>

static void cb_glfw_err(int type, const char *msg) { fprintf(stderr, "[GLFW %d] %s", type, msg); }
static void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods);

vrAssertHandler_t vrAssertHandler = vr_assert;
RemoteControlState *remoteControlState = nullptr;
RemoteControlState *lastRemoteControlState = nullptr;

int main(int argc, char **argv) {
	GLFWwindow *window = nullptr;
	IMonadoConnection *conn = nullptr;
	const char *disableMonadoEnv;
	uint64_t startTime;

	TestSystem ts;
	TestInput ti;

	float lastInputUpdate = 0;
	int inputUpdateCount = 0;

	if (!glfwInit()) {
		fprintf(stderr, "Failed to initialise glfw.\n");
		return 1;
	}

	// From this point on we shouldn't return, and instead goto to err

	glfwSetErrorCallback(cb_glfw_err);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	window = glfwCreateWindow(480, 360, "OpenVR Unofficial Test suite", nullptr, nullptr);

	if (!window) {
		fprintf(stderr, "Failed to create window\n");
		goto err;
	}

	glfwSetKeyCallback(window, key_callback);

	glfwMakeContextCurrent(window);
	glfwSwapInterval(0);

	if (!gladLoadGL()) {
		fprintf(stderr, "Failed to load GLAD\n");
		goto err;
	}

	// Setup OpenVR
	vr::EVRInitError vrInitErr;
	vr::VR_Init(&vrInitErr, vr::VRApplication_Scene);
	if (vrInitErr != vr::VRInitError_None) {
		const char *msg = vr::VR_GetVRInitErrorAsEnglishDescription(vrInitErr);
		fprintf(stderr, "Failed to initialise OpenVR: %s\n", msg);
		goto err;
	}

	// Start our logging hack (uncomment to activate)
	// steamvr_enable_logging_hook();

	if (do_render_setup()) {
		fprintf(stderr, "Failed to initialise renderer\n");
		goto err;
	}

	// Connect to Monado and take control of our virtual input device
#ifdef _WIN32
	fprintf(stderr, "Monado connections not currently supported on Windows\n");
#else
	disableMonadoEnv = getenv("DISABLE_MONADO_REMOTE_CTRL");
	if (disableMonadoEnv && strcasecmp(disableMonadoEnv, "true") == 0) {
		printf("Monado connection disabled by environment variables\n");
	} else {
		if (MonadoConnection::connect_to_monado(&conn, "localhost", 4242)) {
			fprintf(stderr, "Failed to connect to Monado remote port\n");
		}
	}
#endif

	// Run our tests
	ts.RunStartup();
	ti.RunStartup();

	remoteControlState = new RemoteControlState;
	lastRemoteControlState = new RemoteControlState;
	startTime = get_current_time();

	while (!glfwWindowShouldClose(window)) {
		uint64_t currentTimeNs = get_current_time() - startTime;
		float currentTime = currentTimeNs / 1000000000.0f;

		// Update the input data if appropriate
		if (currentTime - lastInputUpdate > 0.5) {
			lastInputUpdate = currentTime;
			inputUpdateCount++;
		}

		*lastRemoteControlState = *remoteControlState;
		remoteControlState->left.btn_a = (int)(inputUpdateCount / 2) % 2 == 0;
		remoteControlState->left.btn_b = (int)(inputUpdateCount / 2.5) % 2 == 0;

		remoteControlState->right.active = (int)(inputUpdateCount / 2.8) % 2 == 0;

		// Slowly move the HMD and controllers around, both to test how long it takes for this data to propagate and to
		// stop SteamVR from thinking we've no longer wearing the HMD.
		glm::vec3 offset = glm::vec3(0, sin(currentTime) / 10, 0);

		if (conn) {
			// We have to check, since we might not have a connection if we're running on real hardware
			conn->set_control_state(vr::Eye_Left, remoteControlState->left.active, remoteControlState->left.btn_a,
			                        remoteControlState->left.btn_b);
			conn->set_control_state(vr::Eye_Right, remoteControlState->right.active, remoteControlState->right.btn_a,
			                        remoteControlState->right.btn_b);

			conn->set_hmd_pose(offset + glm::vec3(0, 1.5, 0), glm::quat(1, 0, 0, 0));
			conn->set_hand_pose(offset + glm::vec3(0.15, 1.5, -0.3), glm::quat(1, 0, 0, 0), vr::Eye_Right);
			conn->set_hand_pose(offset + glm::vec3(-0.5, 1.5, -0.3), glm::quat(1, 0, 0, 0), vr::Eye_Left);
			conn->send_update();
		}

		// Wait for the poses, so if we just changed the values it'll become visible to us this frame
		vr::TrackedDevicePose_t trackedDevicePose[vr::k_unMaxTrackedDeviceCount];
		ASSERT_SVR_ZERO(vr::VRCompositor()->WaitGetPoses(trackedDevicePose, vr::k_unMaxTrackedDeviceCount, nullptr, 0));

		// Draw and submit the image, and swap it into the desktop display
		do_render_draw(window);
		glfwSwapBuffers(window);
		glfwPollEvents();

		// Actually run our tests
		ts.RunPerFrame();

		// Don't update if we just updated the controls since they might lag by one frame and taking
		// that into account in all the tests would be a bit obnoxious.
		// Be sure to run the test that calls xrSyncActions, though.
		ti.RunPerFrameAlways();
		if (currentTime > 1.5)
			ti.RunPerFrame();
	}

	printf("Hello, World!\n");

	//

err:
	vr::VR_Shutdown();

	if (window)
		glfwDestroyWindow(window);
	glfwTerminate();
}

static void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods) {
	// Toggle the skeleton Bind pose wireframe (B for Bind)
	if (key == GLFW_KEY_B && action == GLFW_PRESS)
		toggle_render_bind_wireframe();

	// Draw an axis marker instead of the hands, to figure out if the hands are pointing the wrong way
	if (key == GLFW_KEY_H && action == GLFW_PRESS)
		toggle_render_orientation_hands();
}
