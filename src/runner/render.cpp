//
// Created by ZNix on 09/06/2021.
//

#include "global.h"

#include "asserts.h"
#include "render.h"
#include "render/Model.h"
#include "render/SceneObject.h"
#include "render/SudoFontMeta.h"
#include "render/gltf_loader.h"
#include "render/obj_loader.h"
#include "render/skeletal_input.h"
#include "shaders.h"
#include "stb_image.h"

#include <GLFW/glfw3.h>
#include <fstream>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/string_cast.hpp>
#include <map>
#include <span>
#include <sstream>

static void build_eye(Texture *colour, FBO *fbo);
static void drawHandSkeleton(vr::VRActionHandle_t handle, glm::mat4 parentId, vr::EVREye side);
static void updateHandBones(std::span<glm::mat4> bones, vr::EVREye side, glm::mat4 worldXf);
static std::string getBoneName(HandSkeletonBone bone);
static HandSkeletonBone getBoneIdByName(std::string name);
static void drawText(glm::mat4 toCameraSpace, glm::mat4 transform, float scale, std::string text);

struct HandTrackingData {
	bool valid = false;
	glm::mat4 poses[26] = {};
};
static HandTrackingData leftHandTracking, rightHandTracking;

VBO build_vbo(int size, const void *data) {
	VBO vbo = 0;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
	return vbo;
}

static ShaderSrc compile_shader(const char *name, GLenum type, const char *src) {
	ShaderSrc vert = glCreateShader(type);
	glShaderSource(vert, 1, &src, nullptr);
	glCompileShader(vert);

	int status;
	glGetShaderiv(vert, GL_COMPILE_STATUS, &status);
	if (!status) {
		char msg[512];
		memset(msg, 0, sizeof(msg));
		glGetShaderInfoLog(vert, sizeof(msg) - 1, nullptr, msg);
		fprintf(stderr, "Failed to build %s shader: %s", name, msg);
		abort();
	}

	return vert;
}

static glm::mat4 convert_matrix(const vr::HmdMatrix34_t &src) {
	glm::mat4 mat(1.f);
	for (int x = 0; x < 3; x++) {
		for (int y = 0; y < 4; y++) {
			mat[y][x] = src.m[x][y];
		}
	}
	return mat;
}

static VBO triangle = 0;
static Texture eyeColour = 0;
static FBO eyeFbo = 0;
static Texture fontTex = 0;
static SceneObject *controllerObj;
static SceneObject *coordinateWidget;
static SudoFontMeta *font = nullptr;

static std::vector<glm::mat4> widgetPositions;

static VBO boneVBO = 0;
static GLuint lineShader = 0;
static VAO lineVao = 0;

static GLuint fontShader = 0;

static bool renderBindWireframe = false;
static bool renderOrientationHands = false;

int do_render_setup() {
	float distance = -1;
	float verts[] = {// First triangle
	                 0.0f, 0.0f, distance, 1.0f, 0.0f, distance, 1.1f, 1.1f, distance,

	                 // Second triangle in the distance
	                 0.0f, 0.0f, distance * 2, 1.0f, 0.0f, distance * 2, 1.1f, 1.1f, distance * 2};
	triangle = build_vbo(sizeof(verts), verts);

	glGenBuffers(1, &boneVBO);
	glBindBuffer(GL_ARRAY_BUFFER, boneVBO);
	glBufferData(GL_ARRAY_BUFFER, 16 * 1024, nullptr, GL_DYNAMIC_DRAW); // Way more memory than needed
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Build and configure our VAO
	glGenVertexArrays(1, &lineVao);
	glBindVertexArray(lineVao);

	glBindBuffer(GL_ARRAY_BUFFER, boneVBO);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), nullptr);
	glEnableVertexAttribArray(0);

	glBindVertexArray(0);

	// Compile the shaders
	ShaderSrc vert = compile_shader("vert", GL_VERTEX_SHADER, get_vert_shader_src());
	ShaderSrc frag = compile_shader("frag", GL_FRAGMENT_SHADER, get_frag_shader_src());

	GLuint programme = glCreateProgram();
	glAttachShader(programme, vert);
	glAttachShader(programme, frag);
	glLinkProgram(programme);

	int status;
	glGetProgramiv(programme, GL_LINK_STATUS, &status);
	if (!status) {
		char msg[512];
		memset(msg, 0, sizeof(msg));
		glGetProgramInfoLog(programme, sizeof(msg) - 1, nullptr, msg);
		fprintf(stderr, "Failed to link shader: %s", msg);
		abort();
	}
	glUseProgram(programme);

	glDeleteShader(vert);
	glDeleteShader(frag);

	// Compile the line shaders
	{
		vert = compile_shader("vert", GL_VERTEX_SHADER, get_line_vert_shader_src());
		frag = compile_shader("frag", GL_FRAGMENT_SHADER, get_line_frag_shader_src());

		lineShader = glCreateProgram();
		glAttachShader(lineShader, vert);
		glAttachShader(lineShader, frag);
		glLinkProgram(lineShader);

		glGetProgramiv(lineShader, GL_LINK_STATUS, &status);
		if (!status) {
			char msg[512];
			memset(msg, 0, sizeof(msg));
			glGetProgramInfoLog(lineShader, sizeof(msg) - 1, nullptr, msg);
			fprintf(stderr, "Failed to link shader: %s", msg);
			abort();
		}

		glDeleteShader(vert);
		glDeleteShader(frag);
	}

	// Compile the text shaders
	{
		vert = compile_shader("vert", GL_VERTEX_SHADER, get_font_vert_shader_src());
		frag = compile_shader("frag", GL_FRAGMENT_SHADER, get_font_frag_shader_src());

		fontShader = glCreateProgram();
		glAttachShader(fontShader, vert);
		glAttachShader(fontShader, frag);
		glLinkProgram(fontShader);

		glGetProgramiv(fontShader, GL_LINK_STATUS, &status);
		if (!status) {
			char msg[512];
			memset(msg, 0, sizeof(msg));
			glGetProgramInfoLog(fontShader, sizeof(msg) - 1, nullptr, msg);
			fprintf(stderr, "Failed to link shader: %s", msg);
			abort();
		}

		glDeleteShader(vert);
		glDeleteShader(frag);
	}

	// Build and configure our VAO
	VAO vao = 0;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glBindBuffer(GL_ARRAY_BUFFER, triangle);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), nullptr);
	glEnableVertexAttribArray(0);

	// Create the framebuffers
	build_eye(&eyeColour, &eyeFbo);

	IModel *controllerModel = nullptr;
	// load_obj(&controllerModel, std::string(VRTEST_ASSET_DIR) + "/controller.obj");
	load_gltf(&controllerModel, std::string(VRTEST_ASSET_DIR) + "/skeletal_glove/vr_glove_model.glb",
	          std::string(VRTEST_ASSET_DIR) + "/skeletal_glove/vr_glove_color_red.jpg");
	assert(controllerModel);
	controllerObj = new SceneObject(programme);
	controllerObj->m_model = std::shared_ptr<IModel>(controllerModel);

	IModel *widgetModel = nullptr;
	load_gltf(&widgetModel, std::string(VRTEST_ASSET_DIR) + "/test/coordinate_widget.glb",
	          std::string(VRTEST_ASSET_DIR) + "/test/coordinate_widget.png");
	assert(widgetModel);
	coordinateWidget = new SceneObject(programme);
	coordinateWidget->m_model = std::shared_ptr<IModel>(widgetModel);

	// Load the font metadata
	try {
		std::basic_ifstream<uint8_t> mdInput(std::string(VRTEST_ASSET_DIR) + "Ubuntu-30.sfn", std::ios::binary);
		mdInput.exceptions(std::ios::badbit | std::ios::failbit);
		std::basic_stringstream<uint8_t> tmpBuf;
		tmpBuf << mdInput.rdbuf();
		std::basic_string<uint8_t> dataStr = tmpBuf.str();
		std::vector<uint8_t> mdBytes(dataStr.begin(), dataStr.end());
		font = new SudoFontMeta(std::move(mdBytes));
	} catch (const std::ios::failure &ex) {
		fprintf(stderr, "Error opening font file: %s - %s\n", ex.what(), ex.code().message().c_str());
		return 1;
	}

	// And the font texture
	{
		int width, height;
		std::string fontTexFilename = std::string(VRTEST_ASSET_DIR) + "Ubuntu-30-texture.png";
		uint8_t *data = stbi_load(fontTexFilename.c_str(), &width, &height, nullptr, 4);
		if (data == nullptr) {
			const char *reason = stbi_failure_reason();
			fprintf(stderr, "Failed to load font texture: %s\n", reason);
			return 1;
		}

		glGenTextures(1, &fontTex);
		glBindTexture(GL_TEXTURE_2D, fontTex);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	return 0;
}

void toggle_render_bind_wireframe() { renderBindWireframe = !renderBindWireframe; }

void toggle_render_orientation_hands() { renderOrientationHands = !renderOrientationHands; }

static void build_eye(Texture *colour, FBO *fbo) {
	*colour = 0;
	*fbo = 0;

	uint32_t width = 1000;
	uint32_t height = 1000;
	vr::VRSystem()->GetRecommendedRenderTargetSize(&width, &height);

	width *= 2;

	FBO eye;
	glGenFramebuffers(1, &eye);
	glBindFramebuffer(GL_FRAMEBUFFER, eye);

	Texture tex;
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0);

	Texture depth;
	glGenTextures(1, &depth);
	glBindTexture(GL_TEXTURE_2D, depth);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth, 0);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	*fbo = eye;
	*colour = tex;
}

static void do_render_draw_once(vr::EVREye eye, bool clear) {
	if (clear) {
		float c = sin(glfwGetTime()) * 0.3 + 0.5;
		glClearColor(0.0F, 1.0F, c, 1.0F);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	glm::mat4 poseMats[vr::k_unMaxTrackedDeviceCount];
	vr::TrackedDevicePose_t poses[vr::k_unMaxTrackedDeviceCount];
	ASSERT_SVR_ZERO(vr::VRCompositor()->GetLastPoses(poses, vr::k_unMaxTrackedDeviceCount, nullptr, 0));
	for (int i = 0; i < vr::k_unMaxTrackedDeviceCount; i++) {
		glm::mat4 &mat = poseMats[i];
		mat = convert_matrix(poses[i].mDeviceToAbsoluteTracking);
	}

	float fovLeft, fovRight, fovTop, fovBottom; // NOLINT(readability-isolate-declaration)
	// Values for top and bottom need inverting, see https://github.com/ValveSoftware/openvr/issues/110
	vr::VRSystem()->GetProjectionRaw(eye, &fovLeft, &fovRight, &fovTop, &fovBottom);
	// See https://github.com/ValveSoftware/openvr/wiki/IVRSystem::GetProjectionRaw
	float zFar = 10.f;
	float zNear = 0.01f;
	float idx = 1.0f / (fovRight - fovLeft);
	float idy = 1.0f / (fovBottom - fovTop);
	float idz = 1.0f / (zFar - zNear);
	float sx = fovRight + fovLeft; // NOLINT(readability-identifier-length)
	float sy = fovBottom + fovTop; // NOLINT(readability-identifier-length)
	glm::mat4 projection;
	// clang-format off
	glm::mat4 &p = projection; // NOLINT(readability-identifier-length)
	p[0][0] = 2*idx; p[1][0] = 0;     p[2][0] = sx*idx;    p[3][0] = 0;
	p[0][1] = 0;     p[1][1] = 2*idy; p[2][1] = sy*idy;    p[3][1] = 0;
	p[0][2] = 0;     p[1][2] = 0;     p[2][2] = -zFar*idz; p[3][2] = -zFar*zNear*idz;
	p[0][3] = 0;     p[1][3] = 0;     p[2][3] = -1.0f;     p[3][3] = 0;
	// clang-format on

	glm::mat4 eyeTransform = convert_matrix(vr::VRSystem()->GetEyeToHeadTransform(eye));
	glm::mat4 camTransform = poseMats[0];
	glm::mat4 toCamSpace = projection * glm::inverse(eyeTransform) * glm::inverse(camTransform);

	// Draw a 'pole' of objects so there's something to look at
	for (int y = 0; y < 20; y++) {
		glm::mat4 xf = glm::identity<glm::mat4>();
		xf[3] = {0, y * 0.1, 0, 1};
		coordinateWidget->draw_at(toCamSpace, xf);
	}

	std::vector<std::pair<glm::mat4, std::string>> componentLabels;

	for (int i = 1; i < vr::k_unMaxTrackedDeviceCount; i++) {
		if (!poses[i].bPoseIsValid)
			continue;

		vr::ETrackedControllerRole role = vr::VRSystem()->GetControllerRoleForTrackedDeviceIndex(i);
		HandTrackingData *handTracking = nullptr;
		std::string inputSource;
		if (role == vr::TrackedControllerRole_LeftHand) {
			handTracking = &leftHandTracking;
			inputSource = "/user/hand/left";
		} else if (role == vr::TrackedControllerRole_RightHand) {
			handTracking = &rightHandTracking;
			inputSource = "/user/hand/right";
		}
		if (handTracking && handTracking->valid) {
			vr::EVREye side = role == vr::TrackedControllerRole_LeftHand ? vr::Eye_Left : vr::Eye_Right;
			std::span<glm::mat4> bones(handTracking->poses, std::size(handTracking->poses));
			updateHandBones(bones, side, poseMats[i]);
		}

		controllerObj->draw_at(toCamSpace, poseMats[i]);

		if (renderOrientationHands && !inputSource.empty()) {
			widgetPositions.push_back(poseMats[i]);

			vr::VRInputValueHandle_t src = 0;
			vr::VRInput()->GetInputSourceHandle(inputSource.c_str(), &src);
			if (!src)
				continue;

			vr::ETrackedPropertyError propErr = vr::TrackedProp_Success;
			char modelName[1024];
			memset(modelName, 0, sizeof(modelName));
			vr::VRSystem()->GetStringTrackedDeviceProperty(i, vr::Prop_RenderModelName_String, modelName,
			                                               sizeof(modelName), &propErr);

			std::vector<const char *> names = {
			    vr::k_pch_Controller_Component_Tip,
			    vr::k_pch_Controller_Component_Base,
			    vr::k_pch_Controller_Component_HandGrip,
			};
			for (const char *name : names) {
				vr::RenderModel_ControllerMode_State_t state = {false}; // Function input, not overwritten
				vr::RenderModel_ComponentState_t comp = {};
				bool valid = vr::VRRenderModels()->GetComponentStateForDevicePath(modelName, name, src, &state, &comp);
				if (!valid)
					continue;

				glm::mat4 transform = convert_matrix(comp.mTrackingToComponentLocal);

				widgetPositions.push_back(poseMats[i] * transform);

				componentLabels.push_back({poseMats[i] * transform, name});

				// TESTING
				std::string txStr = glm::to_string(transform);
				// printf("Device %s bone %s: %s\n", inputSource.c_str(), name, txStr.c_str());
			}
		}
	}

	// Draw the hand bones
	vr::VRActionHandle_t handle;
	vr::VRInput()->GetActionHandle("/actions/main/in/LeftHand_Anim", &handle);
	drawHandSkeleton(handle, toCamSpace, vr::Eye_Left);
	vr::VRInput()->GetActionHandle("/actions/main/in/RightHand_Anim", &handle);
	drawHandSkeleton(handle, toCamSpace, vr::Eye_Right);

	// Draw the coordinate system widgets on top of everything else so they can be seen through skin
	glClear(GL_DEPTH_BUFFER_BIT);
	for (const glm::mat4 &pos : widgetPositions) {
		coordinateWidget->draw_at(toCamSpace, pos);
	}
	widgetPositions.clear();

	// Draw the '/pose/raw' pose
	vr::VRActionHandle_t handPose;
	assert(vr::VRInputError_None == vr::VRInput()->GetActionHandle("/actions/single_sided/in/hand_pose", &handPose));
	for (int i = 0; i < 2; i++) {
		vr::EVRInputError err;

		vr::VRInputValueHandle_t handSrc = {};
		const char *name = i == 0 ? "/user/hand/left" : "/user/hand/right";
		err = vr::VRInput()->GetInputSourceHandle(name, &handSrc);
		if (err != vr::VRInputError_None)
			continue;

		vr::InputPoseActionData_t actionData = {};
		err = vr::VRInput()->GetPoseActionDataForNextFrame(handPose, vr::TrackingUniverseStanding,
			&actionData, sizeof(actionData), handSrc);
		if (err != vr::VRInputError_None)
			continue;

		if (!actionData.bActive || !actionData.pose.bDeviceIsConnected)
			continue;

		glm::mat4 pose = convert_matrix(actionData.pose.mDeviceToAbsoluteTracking);

		componentLabels.push_back({pose, "/hand_pose"});
	}

	// Draw the labels for the various components
	float textScale = 0.1f;
	for (const auto &[transform, text] : componentLabels) {
		drawText(toCamSpace, transform, textScale, text);
	}
}

static void drawHandSkeletonImpl(vr::VRActionHandle_t handle, glm::mat4 toCamSpace, vr::EVREye side,
                                 vr::VRBoneTransform_t *bones, vr::EVRSkeletalTransformSpace xfSpace, bool savePoses);

static void drawHandSkeleton(vr::VRActionHandle_t handle, glm::mat4 toCamSpace, vr::EVREye side) {
	// Don't draw the skeletons if the hands aren't being drawn
	if (renderOrientationHands)
		return;

	vr::EVRSkeletalTransformSpace xfSpace = vr::VRSkeletalTransformSpace_Parent;

	if (side == vr::Eye_Left) {
		xfSpace = vr::VRSkeletalTransformSpace_Model;
	}

	vr::VRBoneTransform_t bones[31];
	vr::EVRInputError err = vr::VRInput()->GetSkeletalBoneData(
	    handle, xfSpace, vr::VRSkeletalMotionRange_WithController, bones, std::size(bones));

	if (err != vr::VRInputError_None) {
		return;
	}

	// Draw the real skeleton
	drawHandSkeletonImpl(handle, toCamSpace, side, bones, xfSpace, true);

	// Draw the bind skeleton for comparison
	IGltfModel *model = dynamic_cast<IGltfModel *>(controllerObj->m_model.get());
	if (model && renderBindWireframe) {
		vr::VRBoneTransform_t bindBones[eBone_Count];
		for (int i = 0; i < std::size(bindBones); i++) {
			std::string name = getBoneName((HandSkeletonBone)i);

			size_t targetChar = name.find('*');
			if (targetChar != std::string::npos)
				name.at(targetChar) = 'r';

			glm::mat4 mat = model->GetBoneBindPose(name);
			glm::quat quat(mat);
			bindBones[i] = {
			    .position = {mat[3][0], mat[3][1], mat[3][2], 1.f},
			    .orientation = {quat.w, quat.x, quat.y, quat.z},
			};
		}
		drawHandSkeletonImpl(handle, toCamSpace, side, bindBones, vr::VRSkeletalTransformSpace_Model, false);
	}
}

static void drawHandSkeletonImpl(vr::VRActionHandle_t handle, glm::mat4 toCamSpace, vr::EVREye side,
                                 vr::VRBoneTransform_t *bones, vr::EVRSkeletalTransformSpace xfSpace, bool savePoses) {
	vr::InputPoseActionData_t pose = {};
	vr::ETrackingUniverseOrigin origin = vr::VRCompositor()->GetTrackingSpace();
	vr::VRInput()->GetPoseActionDataForNextFrame(handle, origin, &pose, sizeof(pose), 0);

	glm::mat4 objXf = convert_matrix(pose.pose.mDeviceToAbsoluteTracking);

	// Build the lines storage
	static float linePoints[4 * 2 * 100];
	memset(linePoints, 0, sizeof(linePoints));
	int count = 0;
	auto push = [&](glm::vec4 pos) {
		pos = pos;
		int i = count * 4;
		linePoints[i + 0] = pos.x;
		linePoints[i + 1] = pos.y;
		linePoints[i + 2] = pos.z;
		linePoints[i + 3] = pos.w;
		count++;
	};

	std::optional<glm::mat4> resolvedPoses[31];

	auto pushBone = [&](HandSkeletonBone boneId, std::optional<HandSkeletonBone> parentId) {
		std::optional<glm::mat4> &thisResolved = resolvedPoses[boneId];
		glm::mat4 transform(1.f);

		if (thisResolved) {
			transform = thisResolved.value();
		} else {
			const auto &bone = bones[boneId];
			const auto &pos = bone.position.v;
			glm::vec4 vec(pos[0], pos[1], pos[2], 1.f);
			glm::quat rot(bone.orientation.w, bone.orientation.x, bone.orientation.y, bone.orientation.z);

			// Process our transform to a matrix
			transform[3] = vec;
			transform = transform * glm::toMat4(rot); // Make sure you apply rotation THEN translate

			// If the bones are all relative to the previous one, except for the case of the root bone
			if (xfSpace == vr::VRSkeletalTransformSpace_Parent && boneId != eBone_Root) {
				// Resolve the parentId's pose
				HandSkeletonBone parentBone = parentId.value();
				glm::mat4 parent = resolvedPoses[parentBone].value();

				// Make our transform parent-relative
				transform = parent * transform;
			}

			// Save it for later
			thisResolved = transform;
		}

		push(transform[3]);
	};
	auto drawBone = [&](HandSkeletonBone parent, HandSkeletonBone child) {
		pushBone(parent, {});
		pushBone(child, parent);
	};
	auto drawBones = [&](std::vector<HandSkeletonBone> bones) {
		for (int i = 0; i < bones.size() - 1; i++) {
			drawBone(bones.at(i), bones.at(i + 1));
		}
	};

	drawBone(eBone_Root, eBone_Wrist);

	drawBones({
	    eBone_Wrist,
	    eBone_Thumb0,
	    eBone_Thumb1,
	    eBone_Thumb2,
	    eBone_Thumb3,
	});
	drawBones({
	    eBone_Wrist,
	    eBone_IndexFinger0,
	    eBone_IndexFinger1,
	    eBone_IndexFinger2,
	    eBone_IndexFinger3,
	    eBone_IndexFinger4,
	});
	drawBones({
	    eBone_Wrist,
	    eBone_MiddleFinger0,
	    eBone_MiddleFinger1,
	    eBone_MiddleFinger2,
	    eBone_MiddleFinger3,
	    eBone_MiddleFinger4,
	});
	drawBones({
	    eBone_Wrist,
	    eBone_RingFinger0,
	    eBone_RingFinger1,
	    eBone_RingFinger2,
	    eBone_RingFinger3,
	    eBone_RingFinger4,
	});
	drawBones({
	    eBone_Wrist,
	    eBone_PinkyFinger0,
	    eBone_PinkyFinger1,
	    eBone_PinkyFinger2,
	    eBone_PinkyFinger3,
	    eBone_PinkyFinger4,
	});

	// Draw the coordinate space widgets for each (non-aux) bone
	for (int bone = 0; bone < eBone_Aux_Thumb; bone++) {
		widgetPositions.push_back(objXf * resolvedPoses[bone].value());
	}

	// Draw the skeleton lines
	glDisable(GL_DEPTH_TEST);

	glBindVertexArray(lineVao);
	glBindBuffer(GL_ARRAY_BUFFER, boneVBO);
	glBufferSubData(GL_ARRAY_BUFFER, 0, count * 4 * 4, linePoints);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glUseProgram(lineShader);
	int uniformId = glGetUniformLocation(lineShader, "obj_to_cam_line");
	glUniformMatrix4fv(uniformId, 1, false, glm::value_ptr(toCamSpace * objXf));
	glDrawArrays(GL_LINES, 0, count);

	if (savePoses) {
		// Store the hand data for when we next render the hands
		HandTrackingData *data = side == vr::Eye_Left ? &leftHandTracking : &rightHandTracking;
		data->valid = true;
		for (int i = 0; i < std::size(data->poses); i++) {
			data->poses[i] = resolvedPoses[i].value();
		}
	}
}

void do_render_draw(GLFWwindow *window) {
	vr::Texture_t vrTex = {};
	vrTex.eColorSpace = vr::ColorSpace_Gamma;
	vrTex.eType = vr::TextureType_OpenGL;
	vrTex.handle = (void *)(intptr_t)eyeColour;

	// Render desktop view
	int width, height;
	glfwGetFramebufferSize(window, &width, &height);
	glViewport(0, 0, width, height);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	do_render_draw_once(vr::Eye_Left, true);

	// Render VR views
	uint32_t vrWidth, vrHeight;
	vr::VRSystem()->GetRecommendedRenderTargetSize(&vrWidth, &vrHeight);

	glViewport(0, 0, vrWidth, vrHeight);
	glBindFramebuffer(GL_FRAMEBUFFER, eyeFbo);
	do_render_draw_once(vr::Eye_Left, true);

	glViewport(vrWidth, 0, vrWidth, vrHeight);
	// glBindFramebuffer(GL_FRAMEBUFFER, eyeFbo);
	do_render_draw_once(vr::Eye_Right, false);

	glFlush();

	vr::VRTextureBounds_t bounds = {0.f, 0.f, 0.5f, 1.f};
	ASSERT_SVR_ZERO(vr::VRCompositor()->Submit(vr::Eye_Left, &vrTex, &bounds));
	bounds = {0.5f, 0.f, 1.f, 1.f};
	ASSERT_SVR_ZERO(vr::VRCompositor()->Submit(vr::Eye_Right, &vrTex, &bounds));
}

static std::string getBoneName(HandSkeletonBone bone) {
	static std::map<HandSkeletonBone, std::string> boneNames;
	if (boneNames.empty()) {
		auto addBone = [](std::string name) { boneNames[(HandSkeletonBone)boneNames.size()] = name; };
		addBone("Root");
		addBone("wrist_*");

		addBone("finger_thumb_0_*");
		addBone("finger_thumb_1_*");
		addBone("finger_thumb_2_*");
		addBone("finger_thumb_*_end");

		addBone("finger_index_meta_*");
		addBone("finger_index_0_*");
		addBone("finger_index_1_*");
		addBone("finger_index_2_*");
		addBone("finger_index_*_end");

		addBone("finger_middle_meta_*");
		addBone("finger_middle_0_*");
		addBone("finger_middle_1_*");
		addBone("finger_middle_2_*");
		addBone("finger_middle_*_end");

		addBone("finger_ring_meta_*");
		addBone("finger_ring_0_*");
		addBone("finger_ring_1_*");
		addBone("finger_ring_2_*");
		addBone("finger_ring_*_end");

		addBone("finger_pinky_meta_*");
		addBone("finger_pinky_0_*");
		addBone("finger_pinky_1_*");
		addBone("finger_pinky_2_*");
		addBone("finger_pinky_*_end");

		addBone("finger_thumb_*_aux");
		addBone("finger_index_*_aux");
		addBone("finger_middle_*_aux");
		addBone("finger_ring_*_aux");
		addBone("finger_pinky_*_aux");
		assert(boneNames.size() == eBone_Count);
	}

	return boneNames.at(bone);
}

static HandSkeletonBone getBoneIdByName(std::string name) {
	static std::map<std::string, HandSkeletonBone> boneIds;
	if (boneIds.empty()) {
		for (int i = 0; i < eBone_Count; i++) {
			HandSkeletonBone bone = (HandSkeletonBone)i;
			boneIds[getBoneName(bone)] = bone;
		}
	}
	return boneIds.at(name);
}

static void updateHandBones(std::span<glm::mat4> bones, vr::EVREye side, glm::mat4 worldXf) {

	IGltfModel *model = dynamic_cast<IGltfModel *>(controllerObj->m_model.get());
	if (model == nullptr) {
		return;
	}

	for (int i = 0; i < bones.size(); i++) {
		std::string name = getBoneName((HandSkeletonBone)i);

		// Swap out the '*' for the hand this model corresponds to. Since the model is of the right hand and
		// rendered with the bones flipped around as the left hand, this is always 'r'.
		char sideChar = 'r';
		size_t targetChar = name.find('*');
		if (targetChar != std::string::npos)
			name.at(targetChar) = sideChar;

		glm::mat4 bone = bones[i];

		// Mirror the left hand
		if (side == vr::Eye_Left) {
			glm::mat4 mirror = glm::identity<glm::mat4>();
			mirror[0][0] = -1;
			bone = bone * mirror;

			// Fiddle with the bone rotations separately
			glm::mat4 boneTransform = glm::identity<glm::mat4>();
			if (i == eBone_Wrist) {
				// No transform on the wrist
			} else {
				boneTransform[1][1] = -1;
				boneTransform[2][2] = -1;
			}

			glm::vec4 pos = bone[3];
			bone = bone * boneTransform;
			bone[3] = pos;
		}

		model->SetBone(name, bone);

		// Uncomment this if you're debugging the hand mirroring stuff:
		// widgetPositions.push_back(worldXf * bone);
	}
}

static void drawText(glm::mat4 toCameraSpace, glm::mat4 transform, float textScale, std::string text) {
	glUseProgram(fontShader);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindTexture(GL_TEXTURE_2D, fontTex);

	// Scale before applying any other transform
	transform *= glm::scale(glm::identity<glm::mat4>(), glm::vec3(textScale, textScale, textScale));

	int objToCamLoc = glGetUniformLocation(fontShader, "obj_to_cam");
	int uvSpecLoc = glGetUniformLocation(fontShader, "uv_spec");

	float x = 0; // NOLINT(readability-identifier-length)
	float y = 0; // NOLINT(readability-identifier-length)

	glm::vec2 imageSize(font->GetImageWidth(), font->GetImageHeight());

	for (int i = 0; i < text.length(); i++) {
		SudoFontMeta::CharInfo info = font->GetCharInfo(text.at(i)).value();

		glm::mat4 charTx = glm::identity<glm::mat4>();
		charTx[3][0] = x;
		charTx[3][1] = y;
		charTx = toCameraSpace * transform * charTx;

		// Not a proper matrix! Read by the shader as the first column being offset and the second being scale
		// This avoids the need for a mat3 to handle translations.
		glm::mat2 uvSpec = glm::identity<glm::mat2>();
		uvSpec[0] = glm::vec2(info.PackedX, info.PackedY) / imageSize;
		uvSpec[1] = glm::vec2(info.PackedWidth, info.PackedHeight) / imageSize;

		glUniformMatrix4fv(objToCamLoc, 1, false, glm::value_ptr(charTx));
		glUniformMatrix2fv(uvSpecLoc, 1, false, glm::value_ptr(uvSpec));
		glDrawArrays(GL_TRIANGLES, 0, 6);

		// Advance to the next character position
		// This is broken as it doesn't include the kerning, but eh - it's good enough.
		x += info.XAdvance / imageSize.x;
	}
}
