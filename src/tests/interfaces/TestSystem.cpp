#include "TestSystem.h"

#define TEST_TARGET System

#include "convert.h"
#include "generated_helpers.h"

#include <cstring>
#include <fstream>
#include <json/json.h>
#include <map>

// See GetProjectionMatrix
static glm::mat4 compose_wrong_projection(float fLeft, float fRight, float fTop, float fBottom, float zNear,
                                          float zFar) {
	float idx = 1.0f / (fRight - fLeft);
	float idy = 1.0f / (fBottom - fTop);
	float idz = 1.0f / (zFar - zNear);
	float sx = fRight + fLeft;
	float sy = fBottom + fTop;

	glm::mat4 p;
	// clang-format off
	p[0][0] = 2*idx; p[0][1] = 0;     p[0][2] = sx*idx;    p[0][3] = 0;
	p[1][0] = 0;     p[1][1] = 2*idy; p[1][2] = sy*idy;    p[1][3] = 0;
	p[2][0] = 0;     p[2][1] = 0;     p[2][2] = -zFar*idz; p[2][3] = -zFar*zNear*idz;
	p[3][0] = 0;     p[3][1] = 0;     p[3][2] = -1.0f;     p[3][3] = 0;
	// clang-format on

	// We computed it transposed since I copied the code from the OpenVR wiki and didn't want to
	// fiddle with all the indices.
	// https://github.com/ValveSoftware/openvr/wiki/IVRSystem::GetProjectionRaw
	return glm::transpose(p);
}

enum class TrackedPropertyType {
	Invalid = 0,
	Bool,
	Float,
	Int32,
	Uint64,
	Matrix34,
	Array,
	String,
};

static std::string property_type_to_string(TrackedPropertyType type) {
#define CASE(name)                                                                                                     \
	case TrackedPropertyType::name:                                                                                    \
		return #name
	switch (type) {
		CASE(Bool);
		CASE(Float);
		CASE(Int32);
		CASE(Uint64);
		CASE(Matrix34);
		CASE(Array);
		CASE(String);
	default:
		return "Invalid";
	}
#undef CASE
}

static TrackedPropertyType property_type_from_string(std::string type) {
#define CASE(name)                                                                                                     \
	if (#name == type)                                                                                                 \
		return TrackedPropertyType::name;
	CASE(Bool);
	CASE(Float);
	CASE(Int32);
	CASE(Uint64);
	CASE(Matrix34);
	CASE(Array);
	CASE(String);
	return TrackedPropertyType::Invalid;
#undef CASE
}

struct DeviceInfo {
	std::string properties_filename;
	std::string type;
	vr::ETrackedControllerRole role;
};

static DeviceInfo get_device_info(vr::TrackedDeviceIndex_t dev) {
	char buf[128];
	memset(buf, 0, sizeof(buf));

	vr::ETrackedPropertyError err;
	vr::VRSystem()->GetStringTrackedDeviceProperty(dev, vr::Prop_ControllerType_String, buf, sizeof(buf) - 1, &err);
	assert(vr::TrackedProp_Success == err);
	std::string type = buf;

	// Include the role to differentiate between left and right controllers
	vr::ETrackedControllerRole role = vr::VRSystem()->GetControllerRoleForTrackedDeviceIndex(dev);

	return DeviceInfo{
	    .properties_filename =
	        std::string(VRTEST_ASSET_DIR) + "/dev_properties/" + type + "." + std::to_string(role) + ".json",
	    .type = type,
	    .role = role,
	};
}

static Json::Value load_property_file(vr::TrackedDeviceIndex_t dev) {
	DeviceInfo info = get_device_info(dev);

	Json::Value root;

	std::ifstream input;
	input.exceptions(std::ios::badbit | std::ios::failbit);
	input.open(info.properties_filename, std::ios::binary); // Don't put CRLFs in on Windows
	input >> root;
	input.close();

	return root;
}

static std::pair<Json::Value, TrackedPropertyType> find_property_and_type(vr::TrackedDeviceIndex_t dev,
                                                                          vr::ETrackedDeviceProperty prop) {
	char buf[128];
	memset(buf, 0, sizeof(buf));

	vr::ETrackedPropertyError err;
	vr::VRSystem()->GetStringTrackedDeviceProperty(dev, prop, buf, sizeof(buf) - 1, &err);
	std::string stringValue = buf;

	if (err == vr::TrackedProp_UnknownProperty)
		return std::pair(Json::nullValue, TrackedPropertyType::Invalid);

	if (err != vr::TrackedProp_WrongDataType) {
		assert(vr::TrackedProp_Success == err);
		return std::pair(stringValue, TrackedPropertyType::String);
	}

	vr::HmdMatrix34_t mat = vr::VRSystem()->GetMatrix34TrackedDeviceProperty(dev, prop, &err);
	if (err != vr::TrackedProp_WrongDataType) {
		assert(vr::TrackedProp_Success == err);

		// Call these A and B since we don't know which way around it is
		Json::Value array = Json::arrayValue;
		for (int a = 0; a < 3; a++) {
			for (int b = 0; b < 4; b++) {
				array.append(mat.m[a][b]);
			}
		}

		return std::pair(array, TrackedPropertyType::Matrix34);
	}

	uint64_t int64value = vr::VRSystem()->GetUint64TrackedDeviceProperty(dev, prop, &err);
	if (err != vr::TrackedProp_WrongDataType) {
		assert(vr::TrackedProp_Success == err);
		return std::pair(int64value, TrackedPropertyType::Uint64);
	}

	int32_t int32value = vr::VRSystem()->GetInt32TrackedDeviceProperty(dev, prop, &err);
	if (err != vr::TrackedProp_WrongDataType) {
		assert(vr::TrackedProp_Success == err);
		return std::pair(int32value, TrackedPropertyType::Int32);
	}

	float floatValue = vr::VRSystem()->GetFloatTrackedDeviceProperty(dev, prop, &err);
	if (err != vr::TrackedProp_WrongDataType) {
		assert(vr::TrackedProp_Success == err);
		return std::pair(floatValue, TrackedPropertyType::Float);
	}

	bool boolValue = vr::VRSystem()->GetBoolTrackedDeviceProperty(dev, prop, &err);
	if (err != vr::TrackedProp_WrongDataType) {
		assert(vr::TrackedProp_Success == err);
		return std::pair(boolValue, TrackedPropertyType::Bool);
	}

	// TODO more data types
	printf("TODO implement datatype for property: %d\n", prop);
	return std::pair(Json::nullValue, TrackedPropertyType::Invalid);
}

/**
 * Save a file containing all this device's properties, bar a few things like serial number.
 *
 * This can later be loaded and used for testing. Idea is the property file is committed to Git, then can be used
 * to check what properties OpenComposite is missing.
 *
 * @param dev The device to save the data of
 */
static void save_property_file(vr::TrackedDeviceIndex_t dev) {
	Json::Value propertiesNode = Json::objectValue;

	// Now generate the data for this file
	for (int prop = 0; prop < 8000; prop++) {
		vr::ETrackedDeviceProperty typedProp = (vr::ETrackedDeviceProperty)prop;

		// Ignore a number of properties that we don't want to save for whatever reason
		switch (typedProp) {
			// These look like they might change between steamvr sessions
		case vr::Prop_ParentDriver_Uint64:
		case vr::Prop_CurrentUniverseId_Uint64:
		case vr::Prop_PreviousUniverseId_Uint64:
		case vr::Prop_Audio_DefaultPlaybackDeviceId_String:
		case vr::Prop_Audio_DefaultRecordingDeviceId_String:
		case vr::Prop_Audio_DefaultPlaybackDeviceVolume_Float:
		case vr::Prop_AllWirelessDongleDescriptions_String: // Not sure about this one, but also might be user-unique
		case vr::Prop_DriverProvidedChaperoneVisibility_Bool:
		case vr::Prop_DriverIsDrawingControllers_Bool:
			continue;
			// These are opaque according to the docs
		case vr::Prop_DisplayHiddenArea_Binary_Start:
		case vr::Prop_DisplayHiddenArea_Binary_End:
		case vr::Prop_ParentContainer:
		case vr::Prop_OverrideContainer_Uint64:
			continue;
			// Calibration and device specific stuff
		case vr::Prop_ImuFactoryGyroBias_Vector3:
		case vr::Prop_ImuFactoryGyroScale_Vector3:
		case vr::Prop_ImuFactoryAccelerometerBias_Vector3:
		case vr::Prop_ImuFactoryAccelerometerScale_Vector3:
		case vr::Prop_CameraDistortionFunction_Int32_Array:
		case vr::Prop_CameraDistortionCoefficients_Float_Array:
		case vr::Prop_CameraToHeadTransform_Matrix34:
		case vr::Prop_CameraToHeadTransforms_Matrix34_Array:
		case vr::Prop_CameraFirmwareDescription_String:
		case vr::Prop_CameraFirmwareVersion_Uint64:
		case vr::Prop_LensCenterLeftU_Float:
		case vr::Prop_LensCenterLeftV_Float:
		case vr::Prop_LensCenterRightU_Float:
		case vr::Prop_LensCenterRightV_Float:
		case vr::Prop_TrackingFirmwareVersion_String:
		case vr::Prop_SerialNumber_String: // For privacy reasons, but we should check something reasonable appears
			continue;
			// Installation-dependant
		case vr::Prop_ResourceRoot_String:
		case vr::Prop_DriverProvidedChaperonePath_String:
		case vr::Prop_DriverVersion_String:
		case vr::Prop_GraphicsAdapterLuid_Uint64:
			continue;
			// User-dependant stuff
		case vr::Prop_UserIpdMeters_Float:
		case vr::Prop_UserHeadToEyeDepthMeters_Float: // Is it?
		case vr::Prop_DashboardScale_Float:
		case vr::Prop_ControllerHandSelectionPriority_Int32: // TODO this IS documented and we should check it's
		                                                     //  implemented, but not from the properties file.
		case vr::Prop_DriverRequestsApplicationPause_Bool:
		case vr::Prop_DriverRequestsReducedRendering_Bool:
		case vr::Prop_IsOnDesktop_Bool: // I think this means Dash is open?
			continue;
			// Other bits and pieces that don't really belong in here
		case vr::Prop_DisplayMCImageData_Binary: // Don't attempt to store image data (!) in the properties file
		case vr::Prop_EdidProductID_Int32:
		case vr::Prop_EdidVendorID_Int32:
		case vr::Prop_FPGAVersion_Uint64:
		case vr::Prop_HardwareRevision_String:
		case vr::Prop_HardwareRevision_Uint64:
		case vr::Prop_HmdTrackingStyle_Int32: // Is this lighthouse-vs-constellation-vs-insight-vs-wmr kind of thing?
		case vr::Prop_ImuToHeadTransform_Matrix34:
		case vr::Prop_IpdUIRangeMaxMeters_Float:
		case vr::Prop_IpdUIRangeMinMeters_Float:
		case vr::Prop_InputProfilePath_String: // Specifies a file that OC doesn't have
			continue;
			// For the vive controllers
		case vr::Prop_ConnectedWirelessDongle_String:
		case vr::Prop_ControllerRoleHint_Int32: // TODO Should probably figure this one out, don't want it in the file
		                                        //  since it's swap depending on which hand turned on first.
		case vr::Prop_DeviceBatteryPercentage_Float:
		case vr::Prop_DongleVersion_Uint64:
		case vr::Prop_RadioVersion_Uint64:
		case vr::Prop_RegisteredDeviceType_String: // In the form of "htc/vive_controllerLHR-<SERIAL>"
		case vr::Prop_VRCVersion_Uint64:
		case vr::Prop_ViveSystemButtonFixRequired_Bool:
			continue;
			// The 'invalid' value is just the id=0 property
		case vr::Prop_Invalid:
			continue;
			// Leave everything else alone
		default:
			break;
		}

		const char *propNameC = helpers::EnumToName_ETrackedDeviceProperty(typedProp);
		if (!propNameC) {
			// If an enum is missing, it's probably some internal thing best left alone
			// It seems highly unlikely any game would use this (well, any non-Valve game at least) and it's probably
			// not exactly stable either.
			continue;
		}
		std::string propName = propNameC;

		// Filter out all the display stuff, which goes into a lot of detail that no game will surely ever use
		// Same with firmware, and the icon paths
		if (propName.find("Prop_Display") != std::string::npos || propName.find("Prop_Firmware") != std::string::npos ||
		    propName.find("Prop_NamedIconPath") != std::string::npos) {
			continue;
		}

		const std::pair<Json::Value, TrackedPropertyType> result = find_property_and_type(dev, typedProp);
		const TrackedPropertyType &type = result.second;
		const Json::Value &jsonValue = result.first;

		if (type != TrackedPropertyType::Invalid) {
			propertiesNode[propName] = jsonValue;
			propertiesNode[propName + ".type"] = property_type_to_string(type);
		}
	}

	DeviceInfo info = get_device_info(dev);

	Json::Value root = Json::objectValue;
	root["properties"] = propertiesNode;
	root["role"] = info.role;
	root["type"] = info.type;

	std::ofstream output;
	output.exceptions(std::ios::badbit | std::ios::failbit);
	output.open(info.properties_filename, std::ios::binary); // Don't put CRLFs in on Windows
	output << root;
	output.close();
}

TEST(GetRecommendedRenderTargetSize) {
	uint32_t width = 0;
	uint32_t height = 0;
	vr::VRSystem()->GetRecommendedRenderTargetSize(&width, &height);

	VR_ASSERT_EQ(2372, width);
	VR_ASSERT_EQ(1332, height);

	// As of SteamVR version 1623188486 these segfault on Linux, probably same on Windows
	// uint32_t width2, height2;
	// vr::VRSystem()->GetRecommendedRenderTargetSize(&width2, nullptr);
	// vr::VRSystem()->GetRecommendedRenderTargetSize(nullptr, &height2);
	// vr::VRSystem()->GetRecommendedRenderTargetSize(nullptr, nullptr);
	// VR_ASSERT_EQ(width, width2);
	// VR_ASSERT_EQ(height, height2);
}

TEST(GetProjectionMatrix) {
	float near = 123;
	float far = 456;

	for (int eye = 0; eye < 2; eye++) {
		vr::HmdMatrix44_t vrMatProjection = vr::VRSystem()->GetProjectionMatrix((vr::EVREye)eye, near, far);
		glm::mat4 vrProjection;
		for (int x = 0; x < 4; x++) {
			for (int y = 0; y < 4; y++) {
				// Transpose, since openvr is row-major
				vrProjection[y][x] = vrMatProjection.m[x][y];
			}
		}

		// Compute the matrix based on GetProjectionRaw. Unfortunately there appears to be a bug in the
		// function they use to compute their projection matrix, as they fail to account for the near plane
		// in the Z value calculation: https://github.com/ValveSoftware/openvr/issues/1052
		// I doubt this will ever change, since it would be a very nasty trap when upgrading openvr versions.
		float left, right, up, down;
		vr::VRSystem()->GetProjectionRaw((vr::EVREye)eye, &left, &right, &up, &down);
		glm::mat4 ourProjection = compose_wrong_projection(left, right, up, down, near, far);

		for (int x = 0; x < 4; x++) {
			for (int y = 0; y < 4; y++) {
				// Ignore very small floating-point precision errors
				float delta = ourProjection[y][x] - vrProjection[y][x];
				if (glm::abs(delta) > 0.001)
					VR_ASSERT_EQ(ourProjection[y][x], vrProjection[y][x]);
			}
		}
	}

	// Invalid eye parameter - the resulting view matrix must be the same as the left eye.
	vr::HmdMatrix44_t leftEye = vr::VRSystem()->GetProjectionMatrix(vr::Eye_Left, near, far);
	for (int eye = 2; eye < 5; eye++) {
		vr::HmdMatrix44_t invalidEye = vr::VRSystem()->GetProjectionMatrix((vr::EVREye)2, near, far);
		for (int x = 0; x < 4; x++) {
			for (int y = 0; y < 4; y++) {
				VR_ASSERT_EQ_FLOAT(leftEye.m[y][x], invalidEye.m[y][x]);
			}
		}
	}
}
TEST(GetProjectionRaw) {
	vr::HmdRect2_t projections[2] = {};
	for (int eye = 0; eye < 2; eye++) {
		vr::VRSystem()->GetProjectionRaw((vr::EVREye)eye, &projections[eye].vTopLeft.v[0],
		                                 &projections[eye].vBottomRight.v[0], &projections[eye].vTopLeft.v[1],
		                                 &projections[eye].vBottomRight.v[1]);
	}

	for (int eye = 2; eye < 5; eye++) {
		vr::HmdRect2_t badProjection = {};

		vr::VRSystem()->GetProjectionRaw((vr::EVREye)eye, &badProjection.vTopLeft.v[0],
		                                 &badProjection.vBottomRight.v[0], &badProjection.vTopLeft.v[1],
		                                 &badProjection.vBottomRight.v[1]);

		VR_ASSERT_EQ_FLOAT(projections[0].vBottomRight.v[0], badProjection.vBottomRight.v[0]);
		VR_ASSERT_EQ_FLOAT(projections[0].vBottomRight.v[1], badProjection.vBottomRight.v[1]);
		VR_ASSERT_EQ_FLOAT(projections[0].vTopLeft.v[0], badProjection.vTopLeft.v[0]);
		VR_ASSERT_EQ_FLOAT(projections[0].vTopLeft.v[1], badProjection.vTopLeft.v[1]);
	}
}
TODO_TEST(ComputeDistortion)
TEST(GetEyeToHeadTransform) {
	// Hardcoded as 63mm for now, obviously may change in future Monado versions
	// https://gitlab.freedesktop.org/monado/monado/-/blob/5838d737b3b57947b29fb1538a23deb0d8627e94/src/xrt/state_trackers/steamvr_drv/ovrd_driver.cpp#L755
	float ipd = 0.063;

	for (int eye = 0; eye < 10; eye++) {
		vr::HmdMatrix34_t mat = vr::VRSystem()->GetEyeToHeadTransform((vr::EVREye)eye);

		glm::mat4 ourMat = glm::identity<glm::mat4>();
		ourMat[3][0] = ipd / 2;
		if (eye == vr::Eye_Left || eye >= 2)
			ourMat[3][0] *= -1;

		for (int y = 0; y < 3; y++) {
			for (int x = 0; x < 4; x++) {
				// Ignore very small floating-point precision errors
				// As usual, transpose at this point
				float delta = ourMat[x][y] - mat.m[y][x];
				if (glm::abs(delta) > 0.001)
					VR_ASSERT_EQ(ourMat[x][y], mat.m[y][x]);
			}
		}
	}
}
TODO_TEST(GetTimeSinceLastVsync)
TODO_TEST(GetD3D9AdapterIndex)
TODO_TEST(GetDXGIOutputInfo)
TODO_TEST(GetOutputDevice)
TODO_TEST(IsDisplayOnDesktop)
TODO_TEST(SetDisplayVisibility)
TODO_TEST(GetDeviceToAbsoluteTrackingPose)
TODO_TEST(GetSeatedZeroPoseToStandingAbsoluteTrackingPose)
TODO_TEST(GetRawZeroPoseToStandingAbsoluteTrackingPose)
TODO_TEST(GetSortedTrackedDeviceIndicesOfClass)
TODO_TEST(GetTrackedDeviceActivityLevel)
TEST(ApplyTransform) {
	vr::TrackedDevicePose_t in = {};
	in.bPoseIsValid = true;
	for (int x = 0; x < 3; x++) {
		for (int y = 0; y < 4; y++) {
			in.mDeviceToAbsoluteTracking.m[x][y] = y + x * 0.1f;
		}
		in.vAngularVelocity.v[x] = x * 0.2f;
		in.vVelocity.v[x] = 1 - x * 0.2f;
	}
	in.bDeviceIsConnected = false;
	in.eTrackingResult = vr::TrackingResult_Calibrating_OutOfRange;

	vr::HmdMatrix34_t transform = {};
	transform.m[0][0] = 1;
	transform.m[1][2] = 1;
	transform.m[2][1] = 1;

	transform.m[0][3] = 123;
	transform.m[1][3] = 456;
	transform.m[2][3] = 789;

	vr::TrackedDevicePose_t out = {};
	vr::VRSystem()->ApplyTransform(&out, &in, &transform);

	VR_ASSERT_EQ(in.bDeviceIsConnected, out.bDeviceIsConnected);
	VR_ASSERT_EQ(in.bPoseIsValid, out.bPoseIsValid);
	VR_ASSERT_EQ(in.eTrackingResult, out.eTrackingResult);

	glm::mat4 xform = vr_to_glm(transform);
	// Note this multiplication is flipped, since openvr is all row-major
	// Note quite sure *why* this is the case even after we've transposed both our input
	//  matrices, but it matches anyway.
	glm::mat4 requiredOut = vr_to_glm(in.mDeviceToAbsoluteTracking) * xform;

	for (int x = 0; x < 4; x++) {
		for (int y = 0; y < 3; y++) {
			// Transpose the output matrix while reading from it
			VR_ASSERT_EQ(requiredOut[x][y], out.mDeviceToAbsoluteTracking.m[y][x]);
		}
		in.vAngularVelocity.v[x] = x * 0.2f;
		in.vVelocity.v[x] = 1 - x * 0.2f;
	}
}
TEST(GetTrackedDeviceIndexForControllerRole) {
	std::map<vr::ETrackedControllerRole, int> deviceForRole;

	// Check invalid device IDs
	VR_ASSERT_EQ(vr::TrackedControllerRole_Invalid, vr::VRSystem()->GetControllerRoleForTrackedDeviceIndex(999));

	// Check invalid controller roles
	VR_ASSERT_EQ(-1, vr::VRSystem()->GetTrackedDeviceIndexForControllerRole((vr::ETrackedControllerRole)123));

	// Check what happens when the explicit invalid role is passed to it
	VR_ASSERT_EQ(-1, vr::VRSystem()->GetTrackedDeviceIndexForControllerRole(vr::TrackedControllerRole_Invalid));

	for (int dev = 0; dev < vr::k_unMaxTrackedDeviceCount; dev++) {
		vr::ETrackedControllerRole role = vr::VRSystem()->GetControllerRoleForTrackedDeviceIndex(dev);
		if (!deviceForRole.count(role) && role != vr::TrackedControllerRole_Invalid)
			deviceForRole[role] = dev;
	}

	for (const auto &pair : deviceForRole) {
		int dev = vr::VRSystem()->GetTrackedDeviceIndexForControllerRole(pair.first);
		VR_ASSERT_EQ(pair.second, dev);
	}
}
TODO_TEST(GetControllerRoleForTrackedDeviceIndex)
TODO_TEST(GetTrackedDeviceClass)
TODO_TEST(IsTrackedDeviceConnected)

// TODO combine these tests into one
TODO_TEST(GetArrayTrackedDeviceProperty)
TEST(GetTrackedDeviceProperty) {
	// If this envirionment variable is set, update the property file table
	const char *updatePropFilesEnv = getenv("VRTEST_UPDATE_PROPERTY_FILES");
	bool updateProps = updatePropFilesEnv && updatePropFilesEnv == std::string("YES");

	for (int dev = 0; dev < vr::k_unMaxTrackedDeviceCount; dev++) {
		vr::ETrackedControllerRole role = vr::VRSystem()->GetControllerRoleForTrackedDeviceIndex(dev);
		if (role == vr::TrackedControllerRole_Invalid && dev != 0)
			continue;

		// Update the properties if required
		if (updateProps) {
			save_property_file(dev);
			continue;
		}

		Json::Value root = load_property_file(dev);
		Json::Value &props = root["properties"];

		for (std::string name : props.getMemberNames()) {
			if (name.ends_with(".type"))
				continue;

			vr::ETrackedDeviceProperty prop = helpers::NameToEnum_ETrackedDeviceProperty(name.c_str()).value();
			TrackedPropertyType type = property_type_from_string(props[name + ".type"].asString());
			VR_ASSERT(type != TrackedPropertyType::Invalid);

			Json::Value &value = props[name];
			vr::ETrackedPropertyError err{};

#define PROP_ASSERT_CORE(goodVal, testVal, eqFunc)                                                                     \
	do {                                                                                                               \
		std::string msg = "Line " + std::to_string(__LINE__) + ": Property read " + name + " failed with error " +     \
		                  vr::VRSystem()->GetPropErrorNameFromEnum(err);                                               \
		std::string assertKey = "prop-err-" + name;                                                                    \
		if (!vrAssertHandler(assertKey.c_str(), err == vr::TrackedProp_Success, msg.c_str()))                          \
			break; /* Only out of the switch, not the loop */                                                          \
                                                                                                                       \
		auto testValEval = (testVal);                                                                                  \
		msg = "Line " + std::to_string(__LINE__) + ": Property " + name +                                              \
		      " '" #testVal "' == " #goodVal " failed for '" + val_to_string(testValEval) + "' (expected '" +          \
		      val_to_string(goodVal) + "')";                                                                           \
		assertKey = "prop-value-" + name;                                                                              \
		if (!vrAssertHandler(assertKey.c_str(), eqFunc(goodVal, testValEval), msg.c_str()))                            \
			break; /* Only out of the switch, not the loop */                                                          \
	} while (0)
#define PROP_ASSERT(goodVal, testVal)                                                                                  \
	PROP_ASSERT_CORE(goodVal, testVal, [](auto varA, auto varB) { return varA == varB; })
#define PROP_ASSERT_FLOAT(goodVal, testVal)                                                                            \
	PROP_ASSERT_CORE(goodVal, testVal, [](auto a, auto b) { return std::fabs(a - b) < 0.00001; })

			switch (type) {
			case TrackedPropertyType::String: {
				char buf[128];
				memset(buf, 0, sizeof(buf));
				vr::VRSystem()->GetStringTrackedDeviceProperty(dev, prop, buf, sizeof(buf) - 1, &err);
				PROP_ASSERT(value.asString(), std::string(buf));
				break;
			}
			case TrackedPropertyType::Int32: {
				int32_t intValue = vr::VRSystem()->GetInt32TrackedDeviceProperty(dev, prop, &err);
				PROP_ASSERT(value.asInt(), intValue);
				break;
			}
			case TrackedPropertyType::Uint64: {
				uint64_t u64value = vr::VRSystem()->GetUint64TrackedDeviceProperty(dev, prop, &err);
				PROP_ASSERT(value.asUInt64(), u64value);
				break;
			}
			case TrackedPropertyType::Float: {
				float floatValue = vr::VRSystem()->GetFloatTrackedDeviceProperty(dev, prop, &err);
				// TODO move to PROP_ASSERT
				PROP_ASSERT_FLOAT(value.asFloat(), floatValue);
				break;
			}
			case TrackedPropertyType::Bool: {
				bool boolValue = vr::VRSystem()->GetBoolTrackedDeviceProperty(dev, prop, &err);
				PROP_ASSERT(value.asBool(), boolValue);
				break;
			}
			case TrackedPropertyType::Matrix34: {
				vr::HmdMatrix34_t mat = vr::VRSystem()->GetMatrix34TrackedDeviceProperty(dev, prop, &err);
				int arrIdx = 0;
				for (int a = 0; a < 3; a++) {
					for (int b = 0; b < 4; b++) {
						PROP_ASSERT_FLOAT(value[arrIdx].asFloat(), mat.m[a][b]);
						arrIdx++;
					}
				}
				break;
			}
			default: {
				std::string msg =
				    "Unknown property type for property " + std::to_string(prop) + ": " + property_type_to_string(type);
				vrAssertHandler(TEST_KEY, false, msg.c_str());
				break;
			}
			}

#undef PROP_ASSERT
#undef PROP_ASSERT_FLOAT
#undef PROP_ASSERT_CORE
		}
	}
}

TEST(GetPropErrorNameFromEnum) {
	for (int i = -1; i <= 15; i++) {
		std::string name = vr::VRSystem()->GetPropErrorNameFromEnum((vr::ETrackedPropertyError)i);
		std::string required;
		switch (i) {
		case -1:
			required = "Unknown property error (-1)";
			break;
		case vr::TrackedProp_Success:
			required = "TrackedProp_Success";
			break;
		case vr::TrackedProp_WrongDataType:
			required = "TrackedProp_WrongDataType";
			break;
		case vr::TrackedProp_WrongDeviceClass:
			required = "TrackedProp_WrongDeviceClass";
			break;
		case vr::TrackedProp_BufferTooSmall:
			required = "TrackedProp_BufferTooSmall";
			break;
		case vr::TrackedProp_UnknownProperty:
			required = "TrackedProp_UnknownProperty";
			break;
		case vr::TrackedProp_InvalidDevice:
			required = "TrackedProp_InvalidDevice";
			break;
		case vr::TrackedProp_CouldNotContactServer:
			required = "TrackedProp_CouldNotContactServer";
			break;
		case vr::TrackedProp_ValueNotProvidedByDevice:
			required = "TrackedProp_ValueNotProvidedByDevice";
			break;
		case vr::TrackedProp_StringExceedsMaximumLength:
			required = "TrackedProp_StringExceedsMaximumLength";
			break;
		case vr::TrackedProp_NotYetAvailable:
			required = "TrackedProp_NotYetAvailable";
			break;
		case vr::TrackedProp_PermissionDenied:
			required = "TrackedProp_PermissionDenied";
			break;
		case vr::TrackedProp_InvalidOperation:
			required = "TrackedProp_InvalidOperation";
			break;
		case vr::TrackedProp_CannotWriteToWildcards:
			required = "TrackedProp_CannotWriteToWildcards";
			break;
		case vr::TrackedProp_IPCReadFailure:
			required = "TrackedProp_IPCReadFailure";
			break;
		case vr::TrackedProp_OutOfMemory:
			required = "TrackedProp_OutOfMemory";
			break;
		case vr::TrackedProp_InvalidContainer:
			required = "TrackedProp_InvalidContainer";
			break;
		}
		VR_ASSERT_EQ(required, name);
	}
}

TODO_TEST(PollNextEvent)
TODO_TEST(PollNextEventWithPose)
TODO_TEST(GetEventTypeNameFromEnum)
TODO_TEST(GetHiddenAreaMesh)
TEST_RUN_GROUP(PerFrame)
TEST(GetControllerState) {
	// At the moment, GetControllerState doesn't return any information from Monado. Need something like this in
	// the main input file (see the htc driver as an example):
	// "legacy_binding" :  "{monado}/input/legacy_bindings_khr_simple.json",

	// if (!remoteControlState)
	// 	return;

	// for (int side = 0; side < 2; side++) {
	// 	int device = vr::VRSystem()->GetTrackedDeviceIndexForControllerRole(
	// 	    (vr::ETrackedControllerRole)(vr::TrackedControllerRole_LeftHand + side));
	// 	if (device == -1)
	// 		continue;

	// 	RemoteHandControllerState &real = side == vr::Eye_Left ? remoteControlState->left : remoteControlState->right;

	// 	vr::VRControllerState_t state;
	// 	VR_ASSERT(vr::VRSystem()->GetControllerState(device, &state, sizeof(state)));

	// 	printf("%x\t%lx\n", real.menu, state.ulButtonPressed);
	// 	VR_ASSERT_EQ(real.menu, state.ulButtonPressed & vr::ButtonMaskFromId(vr::k_EButton_ApplicationMenu));
	// }
}
TODO_TEST(GetControllerStateWithPose)
TODO_TEST(TriggerHapticPulse)
TEST(GetButtonIdNameFromEnum) {
	// Go through all the valid button values and one more
	for (int id = 0; id < vr::k_EButton_Max + 1; id++) {
		const char *name = vr::VRSystem()->GetButtonIdNameFromEnum((vr::EVRButtonId)id);
		std::string correct;

#define BTN_NAME(id_name)                                                                                              \
	case vr::id_name:                                                                                                  \
		correct = #id_name;                                                                                            \
		break;

		switch (id) {
			BTN_NAME(k_EButton_System)
			BTN_NAME(k_EButton_ApplicationMenu)
			BTN_NAME(k_EButton_Grip)
			BTN_NAME(k_EButton_DPad_Left)
			BTN_NAME(k_EButton_DPad_Up)
			BTN_NAME(k_EButton_DPad_Right)
			BTN_NAME(k_EButton_DPad_Down)
			BTN_NAME(k_EButton_A)

			BTN_NAME(k_EButton_ProximitySensor)

			BTN_NAME(k_EButton_Axis0)
			BTN_NAME(k_EButton_Axis1)
			BTN_NAME(k_EButton_Axis2)
			BTN_NAME(k_EButton_Axis3)
			BTN_NAME(k_EButton_Axis4)
		default:
			char tmp[1024];
			sprintf(tmp, "Unknown EVRButtonId (%d)", id);
			correct = tmp;
			break;
		}

#undef BTN_NAME

		VR_ASSERT_EQ(correct, std::string(name));
	}
}
TEST(GetControllerAxisTypeNameFromEnum) {
	for (int id = 0; id < 50; id++) {
		const char *name = vr::VRSystem()->GetControllerAxisTypeNameFromEnum((vr::EVRControllerAxisType)id);
		std::string correct;

		switch (id) {
		case vr::k_eControllerAxis_None:
			correct = "k_eControllerAxis_None";
			break;
		case vr::k_eControllerAxis_TrackPad:
			correct = "k_eControllerAxis_TrackPad";
			break;
		case vr::k_eControllerAxis_Joystick:
			correct = "k_eControllerAxis_Joystick";
			break;
		case vr::k_eControllerAxis_Trigger:
			correct = "k_eControllerAxis_Trigger";
			break;
		default:
			char tmp[1024];
			sprintf(tmp, "Unknown EVRControllerAxisType (%d)", id);
			correct = tmp;
			break;
		}

		VR_ASSERT_EQ(correct, std::string(name));
	}
}
TODO_TEST(IsInputAvailable)
TODO_TEST(IsSteamVRDrawingControllers)
TODO_TEST(ShouldApplicationPause)
TODO_TEST(ShouldApplicationReduceRenderingWork)
TODO_TEST(PerformFirmwareUpdate)
TODO_TEST(AcknowledgeQuit_Exiting)
TODO_TEST(GetAppContainerFilePaths)
TODO_TEST(GetRuntimeVersion)
// TODO test the weird hmd factory thing for proton exists (don't call it on pain of crashing)
