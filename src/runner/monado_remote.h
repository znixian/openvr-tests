//
// Created by znix on 22/06/2021.
//

#pragma once

#include <stdint.h>

class IMonadoConnection {
  public:
	virtual ~IMonadoConnection();

	virtual void set_hmd_pose(glm::vec3 pos, glm::quat rot) = 0;
	virtual void set_hand_pose(glm::vec3 pos, glm::quat rot, vr::EVREye eye) = 0;
	virtual void send_update() = 0;

	virtual void set_control_state(vr::EVREye side, bool active, bool btn_a, bool btn_b) = 0;
};

#ifndef _WIN32

class MonadoConnection : public IMonadoConnection {
  public:
	static int connect_to_monado(IMonadoConnection **out, const char *ip_addr, uint16_t port);

	~MonadoConnection() override;

	void set_hmd_pose(glm::vec3 pos, glm::quat rot) override;
	void set_hand_pose(glm::vec3 pos, glm::quat rot, vr::EVREye eye) override;
	void send_update() override;

	void set_control_state(vr::EVREye side, bool active, bool btn_a, bool btn_b) override;

  private:
	int m_conn_fd = -1;

	explicit MonadoConnection();

	int read_data(struct r_remote_data *data) const;
	int write_data(const struct r_remote_data *data) const;

	struct GlmPose {
		glm::vec3 position;
		glm::quat rotation = glm::quat(1, 0, 0, 0);

		struct xrt_pose to_monado() const;
		void apply_inputs(struct r_remote_controller_data &data) const;

		bool active = false, btn_a = false, btn_b = false;
	};

	GlmPose m_hmd_pose;
	GlmPose m_hand_poses[2]; // Indexed by vr::EVREye

	/**
	 * For hand tracking. Each hand has five values from 0-1, indexed as:
	 * 0 = little
	 * 1 = ring
	 * 2 = middle
	 * 3 = index
	 * 4 = thumb
	 */
	float m_finger_curls[2][5] = {};
};

#endif
