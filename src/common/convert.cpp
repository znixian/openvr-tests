//
// Created by znix on 27/06/2021.
//

#include "convert.h"

#include <glm/gtc/matrix_transform.hpp>

glm::mat4 vr_to_glm(const vr::HmdMatrix34_t &mat) {
	glm::mat4 m = glm::identity<glm::mat4>();

	// Transpose the matrix while we're here, since openvr uses row-major stuff
	for (int x = 0; x < 4; x++) {
		for (int y = 0; y < 3; y++) {
			m[x][y] = mat.m[y][x];
		}
	}

	return m;
}
