//
// Created by znix on 23/06/2021.
//

#include "global.h"

#include "SceneObject.h"

SceneObject::SceneObject(int shader) : shader(shader) {
	m_shaderVertMult = glGetUniformLocation(shader, "obj_to_cam");
	m_objToWorld = glGetUniformLocation(shader, "obj_to_world");
}

void SceneObject::draw(glm::mat4 projection) { draw_at(projection, transform); }

void SceneObject::draw_at(glm::mat4 projection, glm::mat4 customTransform) {
	glBindVertexArray(m_model->GetVAO());
	glUseProgram(shader);
	glUniformMatrix4fv(m_shaderVertMult, 1, false, glm::value_ptr(projection * customTransform));
	glUniformMatrix4fv(m_objToWorld, 1, false, glm::value_ptr(customTransform));
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LESS);
	m_model->Draw(shader);
}
