//
// Created by znix on 29/03/2022.
//

#pragma once

#include "openvr.h"
#include <string>

void steamvr_enable_logging_hook();

std::string lookup_handle_name(vr::VRInputValueHandle_t handle);
