cmake_minimum_required(VERSION 3.16.3)
cmake_policy(SET CMP0048 NEW)
project(OpenVR-TestSuite VERSION 1)

set(CMAKE_CXX_STANDARD 20)

# A variable to set a custom monado_remote_def file, for bisecting when using newer Monado versions.
# Leave at default to use the built-in one.
set(MONADO_DEFS_REMOTE_PATH "" CACHE FILEPATH "Custom monado_remote_def.h path")
if (NOT ${MONADO_DEFS_REMOTE_PATH} EQUAL "")
    set(MONADO_DEFS_REMOTE_PATH_INTERNAL "${MONADO_DEFS_REMOTE_PATH}")
else ()
    set(MONADO_DEFS_REMOTE_PATH_INTERNAL "${CMAKE_SOURCE_DIR}/src/runner/monado_remote_def.h")
endif ()

# Build GLFW
if (WIN32)
    add_subdirectory(libs/glfw)
else ()
    find_package(PkgConfig REQUIRED)
    pkg_search_module(GLFW REQUIRED glfw3)

    add_library(glfw INTERFACE)
    target_link_libraries(glfw INTERFACE -l${GLFW_LIBRARIES}) # HACK otherwise it thinks we mean the glfw we just made
    target_include_directories(glfw INTERFACE ${GLFW_INCLUDE_DIRS})
endif ()

# Build GLAD
add_library(glad STATIC
        libs/glad/src/glad.c
        libs/glad/include/glad/glad.h
        libs/glad/include/KHR/khrplatform.h
        )
target_include_directories(glad PUBLIC libs/glad/include)
if (NOT DEFINED WIN32)
    # Dynamic linking stuff
    target_link_libraries(glad PUBLIC dl)
endif ()

# Build the OpenVR loader
# See src/openvrfixes/README.md
add_library(openvr SHARED
        libs/openvr/src/jsoncpp.cpp
        libs/openvr/src/openvr_api_public.cpp
        libs/openvr/src/vrcore/dirtools_public.cpp
        libs/openvr/src/vrcore/envvartools_public.cpp
        libs/openvr/src/vrcore/hmderrors_public.cpp
        libs/openvr/src/vrcore/pathtools_public.cpp
        libs/openvr/src/vrcore/sharedlibtools_public.cpp
        libs/openvr/src/vrcore/strtools_public.cpp
        libs/openvr/src/vrcore/vrpathregistry_public.cpp
        src/openvrfixes/assert.cpp
        )
target_include_directories(openvr PRIVATE libs/openvr/src libs/openvr/src/vrcore src/openvrfixes)
target_include_directories(openvr PUBLIC libs/openvr/headers)
target_compile_definitions(openvr PRIVATE -DVR_API_PUBLIC)
target_compile_definitions(openvr PRIVATE -DVR_API_EXPORT)
if (NOT WIN32)
    # Prevent the non-exported JSON symbols from colliding since ELF has a global function namespace
    target_compile_options(openvr PRIVATE -fvisibility=hidden)
endif ()
if (WIN32)
    if (CMAKE_SIZEOF_VOID_P EQUAL 8)
        target_compile_definitions(openvr PRIVATE -DWIN64)
    endif ()
    target_compile_definitions(openvr PRIVATE -DWIN32)
else ()
    target_compile_definitions(openvr PRIVATE -DLINUX -DLINUX64 -DPOSIX)
endif ()

# Build GLM
add_subdirectory(libs/glm)

# Build Subhook, which we can use to poke SteamVR for debugging
add_library(subhook STATIC
        libs/subhook/subhook.c
        )
target_compile_definitions(subhook PUBLIC -DSUBHOOK_STATIC) # Static-linked so don't dllexport/dllimport
target_include_directories(subhook PUBLIC libs/subhook)

# Build jsoncpp
add_library(jsoncpp STATIC
        libs/jsoncpp/src/json_reader.cpp
        libs/jsoncpp/src/json_writer.cpp
        libs/jsoncpp/src/json_value.cpp
        )
target_include_directories(jsoncpp PRIVATE libs/jsoncpp/src)
target_include_directories(jsoncpp PUBLIC libs/jsoncpp/include)

# Build tinygltf
add_library(tinygltf STATIC
		libs/tinygltf/tiny_gltf.cc
		)
target_include_directories(tinygltf PRIVATE libs/tinygltf)
target_include_directories(tinygltf PUBLIC libs/tinygltf/include)

# Build our common library of bits and pieces
add_library(common STATIC
        src/common/convert.cpp
        src/common/vrtime.cpp
        )
target_include_directories(common PUBLIC src/common)
target_link_libraries(common PUBLIC openvr glm)

# Build the collection of tests
# These are separate so we can easily build other tools around them later

find_package(Python3 COMPONENTS Interpreter)
add_custom_command(OUTPUT ${CMAKE_BINARY_DIR}/gen_test_hdr/test_meta.gen.h
        MAIN_DEPENDENCY src/tests/codegen/testgen.py
        COMMAND ${Python3_EXECUTABLE} ARGS ${CMAKE_SOURCE_DIR}/src/tests/codegen/testgen.py ${CMAKE_BINARY_DIR}/gen_test_hdr test_meta.gen.h)

add_custom_command(OUTPUT ${CMAKE_BINARY_DIR}/gen_src/helpers.gen.cpp
        MAIN_DEPENDENCY src/tests/codegen/helpergen.py
        COMMAND ${Python3_EXECUTABLE} ARGS ${CMAKE_SOURCE_DIR}/src/tests/codegen/helpergen.py ${CMAKE_BINARY_DIR}/gen_src/helpers.gen.cpp)

add_library(test-suite STATIC
        ${CMAKE_BINARY_DIR}/gen_test_hdr/test_meta.gen.h
        src/tests/tests.cpp src/tests/tests.h
        src/tests/interfaces/TestApplications.cpp
        src/tests/interfaces/TestChaperone.cpp
        src/tests/interfaces/TestChaperoneSetup.cpp
        src/tests/interfaces/TestCompositor.cpp
        src/tests/interfaces/TestExtendedDisplay.cpp
        src/tests/interfaces/TestInput.cpp
        src/tests/interfaces/TestOverlay.cpp
        src/tests/interfaces/TestOverlayView.cpp
        src/tests/interfaces/TestRenderModels.cpp
        src/tests/interfaces/TestScreenshots.cpp
        src/tests/interfaces/TestSettings.cpp
        src/tests/interfaces/TestSystem.cpp
        src/tests/testctrl.cpp src/tests/testctrl.h
        src/tests/generated_helpers.h ${CMAKE_BINARY_DIR}/gen_src/helpers.gen.cpp
        )
target_include_directories(test-suite PUBLIC ${CMAKE_BINARY_DIR}/gen_test_hdr)
target_include_directories(test-suite PUBLIC src/tests)
target_compile_definitions(test-suite PUBLIC -DVRTEST_ASSET_DIR=\"${CMAKE_CURRENT_SOURCE_DIR}/assets/\")
target_link_libraries(test-suite PUBLIC openvr glm common jsoncpp)

# The runner itself
add_executable(openvr-test-runner
        src/runner/main.cpp
        src/runner/render.cpp src/runner/render.h
        src/runner/shaders.cpp src/runner/shaders.h
        src/runner/asserts.cpp src/runner/asserts.h
        src/runner/monado_remote.cpp src/runner/monado_remote.h
        src/runner/render/Model.cpp src/runner/render/Model.h
        src/runner/render/SceneObject.cpp src/runner/render/SceneObject.h
        src/runner/render/obj_loader.cpp src/runner/render/obj_loader.h
        src/runner/render/gltf_loader.cpp src/runner/render/gltf_loader.h
        src/runner/render/skeletal_input.cpp src/runner/render/skeletal_input.h
        src/runner/render/SudoFontMeta.cpp src/runner/render/SudoFontMeta.h
        src/runner/steamvr_dbg.cpp src/runner/steamvr_dbg.h
        )
target_include_directories(openvr-test-runner PRIVATE src/runner)

set_source_files_properties(src/runner/monado_remote.cpp PROPERTIES COMPILE_FLAGS
        "-DMONADO_DEFS_REMOTE_PATH=\"${MONADO_DEFS_REMOTE_PATH_INTERNAL}\" -include \"${MONADO_DEFS_REMOTE_PATH_INTERNAL}\""
        )

target_link_libraries(openvr-test-runner PUBLIC test-suite glfw glad openvr glm common subhook tinygltf)

if (WIN32)
    target_link_libraries(openvr-test-runner PRIVATE opengl32.lib)
endif ()
