#!/bin/env python3
import re
import sys
from pathlib import Path
from typing import Callable, Dict, List, Optional, Tuple

TARGET_ENUMS = ["ETrackedDeviceProperty"]
ENUM_START_REGEX = re.compile('^enum\\s+(?P<name>\\w+)\\s*{?$')
ENUM_ENTRY = re.compile('^(?P<name>\\w+)\\s*(?:=\\s*(?P<value>[\\-0-9x]+)\\s*)?,?$')

# The root of the project - that is, next to .git
proj_dir = Path(__file__).absolute().parents[3]

# Make the directory for the output file
out_file = Path(sys.argv[1]).absolute()
out_dir = out_file.parent

if not out_dir.exists():
    out_dir.mkdir()


class ParserStateMachine:
    _line_handler: Callable = None
    _current_enum: Optional[List[Tuple[str, int]]] = None
    _current_enum_name: Optional[str] = None

    enums: Dict[str, List[Tuple[str, int]]]

    def __init__(self):
        self._line_handler = self._searching
        self.enums = dict()

    def handle_line(self, line: str):
        self._line_handler(line)

    def _searching(self, line: str):
        match = ENUM_START_REGEX.match(line)
        if match is not None:
            name = match.group("name")

            # If we aren't looking for this enum, ignore it
            if name not in TARGET_ENUMS:
                return

            assert name not in self.enums  # Check for a duplicate enum

            self._current_enum = []
            self._current_enum_name = name
            self.enums[name] = self._current_enum

            self._line_handler = self._inside_enum

    def _inside_enum(self, line: str):
        # Closing bracket marks the end of the enum
        if "}" in line:
            self._current_enum = None
            self._current_enum_name = None
            self._line_handler = self._searching
            return

        # Ignore the opening bracket
        if line == "{":
            return

        # Parse this enum value
        match = ENUM_ENTRY.match(line)
        if not match:
            raise Exception(f"Failed to parse enum {self._current_enum_name}: '{line}'")

        name = match.group("name")
        value = int(match.group("value"))

        self._current_enum.append((name, value))


parser = ParserStateMachine()


# Pull enums out of the openvr.h file
def parse_openvr_header():
    with open(proj_dir / "libs/openvr/headers/openvr.h", "r") as fi:
        for line in fi:
            # Chop off any comments
            line = line.split("//", 1)[0]

            # Remove any leading or trailing whitespace
            line = line.strip()

            # Fast-path skip empty lines
            if line == "":
                continue

            parser.handle_line(line)


def generate_main_file():
    with open(out_file, "w") as fi:
        fi.write("""
// Generated file (by helpergen.py) - DO NOT EDIT
// YOUR CHANGES WILL BE ERASED UPON RECOMPILATION

#include <generated_helpers.h>
#include <string.h>

namespace helpers {
        """.strip() + "\n")

        for enum in TARGET_ENUMS:
            enum_values = parser.enums[enum]

            fi.write(f"\n// Helpers for enum {enum}\n")

            fi.write(f"const char *EnumToName_{enum}(vr::{enum} value) {'{'}\n")
            fi.write(f"\tswitch(value) {'{'}\n")
            for name, _ in enum_values:
                fi.write(f"\t\tcase vr::{name}: return \"{name}\";\n")
            fi.write(f"\t\tdefault: return nullptr; // No known enum value\n")
            fi.write(f"\t{'}'}; // End of switch\n")
            fi.write(f"{'}'} // End of enum-to-name\n")

            fi.write(f"std::optional<vr::{enum}> NameToEnum_{enum}(const char *name) {'{'}\n")
            for name, _ in enum_values:
                fi.write(f"\tif (strcmp(name, \"{name}\") == 0) return vr::{name};\n")
            fi.write(f"\treturn std::optional<vr::{enum}>(); // No known enum value\n")
            fi.write(f"{'}'} // End of name-to-enum\n\n")

        fi.write("""
}; // End of namespace 'helpers'
        """.strip() + "\n")


parse_openvr_header()
generate_main_file()
