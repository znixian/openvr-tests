//
// Created by ZNix on 20/06/2021.
//

#include "tests.h"

template <> std::string val_to_string(const std::string &value) { return value; }

template <> std::string val_to_string(const glm::vec3 &value) {
	return std::to_string(value.x) + "," + std::to_string(value.y) + "," + std::to_string(value.z);
}
template <> std::string val_to_string(const glm::vec4 &value) {
	return std::to_string(value.x) + "," + std::to_string(value.y) + "," + std::to_string(value.z) +
	       ",w=" + std::to_string(value.w);
}
