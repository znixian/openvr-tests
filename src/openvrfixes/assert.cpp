//
// Created by ZNix on 13/06/2021.
//

#include "vrcore/assert.h"

#include <stdio.h>
#include <stdlib.h>

void AssertMsg(bool valid, const char *msg) {
	if (valid)
		return;

	fprintf(stderr, "OpenVR Lib Assertion failed: %s", msg);
	abort();
}
