//
// Created by znix on 23/06/2021.
//

#pragma once

#include <vector>

struct ModelVert {
	glm::vec3 pos;
	glm::vec3 norm;
};

struct ModelTri {
	int a, b, c;
};

class IModel {
  public:
	virtual ~IModel();

	virtual VAO GetVAO() = 0;
	virtual void Draw(GLuint shader) = 0;
};

class Model : public IModel {
  public:
	explicit Model();
	Model(const Model &) = delete;
	Model &operator=(const Model &other) = delete;

	VAO get_or_build_vao();

	VAO GetVAO() override { return get_or_build_vao(); }
	void Draw(GLuint shader) override;

	std::vector<ModelVert> verts;
	std::vector<ModelTri> tris;

  private:
	std::optional<VAO> m_cachedVao;
};
