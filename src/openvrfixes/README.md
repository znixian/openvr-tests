# OpenVR Fixes

These are some fixes to make OpenVR build outside of Valve.

Note: renamed vrcommon to vrcore since Valve added includes referencing that. Probably that's what it's called
internally, and their buildscripts have probably (haven't actually tried building it but can't see any other
occurrences of vrcore in their repo) have been broken for the last four months. Maybe I should file a report?

## Licence

In case it's useful to someone else, this code (but not that in other parts of the repo) is licenced under CC-0 (do whatever you want without credit).
