//
// Created by znix on 29/03/2022.
//

#ifndef _WIN32

#include "steamvr_dbg.h"
#include "subhook.h"

#include <cassert>
#include <cerrno>
#include <cstdarg>
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <dlfcn.h>

void hook_log(void *_this, int log_level, char *msg, va_list args) {
	char buf[1000];
	memset(buf, 0, sizeof(buf));
	int count = vsnprintf(buf, sizeof(buf) - 1, msg, args);

	// Trim the trailing newline if present
	if (count > 0 && buf[count - 1] == '\n') {
		buf[count - 1] = 0;
	}

	fprintf(stderr, "INTERNAL: %s\n", buf);
}

void hook_log_finer(void *_this, void *handle, char *msg, ...) {
	char buf[1000];
	memset(buf, 0, sizeof(buf));
	va_list args;
	va_start(args, msg);
	int count = vsnprintf(buf, sizeof(buf) - 1, msg, args);
	va_end(args);

	// Trim the trailing newline if present
	if (count > 0 && buf[count - 1] == '\n') {
		buf[count - 1] = 0;
	}

	fprintf(stderr, "INTERNAL VERBOSE: %p - %s\n", handle, buf);
}

void steamvr_enable_logging_hook() {
	void *handle = dlopen("/home/znix/.local/share/Steam/steamapps/common/SteamVR/bin/linux64/vrclient.so",
	                      RTLD_NOLOAD | RTLD_LAZY);
	assert(handle);
	uint64_t hmd = (uint64_t)dlsym(handle, "HmdSystemFactory");
	uint64_t log = hmd + 507568;
	uint64_t logFiner = hmd + 254224;
	dlclose(handle);
	(new subhook::Hook())->Install((void *)log, (void *)hook_log, subhook::HookFlag64BitOffset);
	(new subhook::Hook())->Install((void *)logFiner, (void *)hook_log_finer, subhook::HookFlag64BitOffset);
}

std::string lookup_handle_name(vr::VRInputValueHandle_t handle) {
	uint64_t vrInput = (uint64_t)vr::VRInput();
	uint64_t hmdSystemLatest = *(uint64_t *)(vrInput + 0x18);
	uint64_t hmdSystemLatestGetPaths = *(uint64_t *)(*(uint64_t *)hmdSystemLatest + 0x38);
	uint64_t pathManager = ((uint64_t(*)(uint64_t))hmdSystemLatestGetPaths)(hmdSystemLatest);

	// uint64_t pathManagerStringToHandle = *(uint64_t *)(*(uint64_t *)pathManager + 0x10);
	// vr::VRInputValueHandle_t src3 = 1;
	// int result = ((int (*)(uint64_t, vr::VRInputValueHandle_t *, const char *))pathManagerStringToHandle)(
	//     pathManager, &src3, "/user/hand/left/input/select");
	// VR_ASSERT_EQ(0, result);
	// VR_ASSERT_EQ(src, src3);

	uint64_t pathManagerHandleToString = *(uint64_t *)(*(uint64_t *)pathManager + 0x18);
	char bufName[1024];
	int result = ((int (*)(uint64_t, vr::VRInputValueHandle_t, char *, int, int *))pathManagerHandleToString)(
	    pathManager, handle, bufName, sizeof(bufName) - 1, nullptr);
	assert(result == 0);

	return bufName;
}

#endif
