#include "TestInput.h"

#ifndef _WIN32
#include <unistd.h>
#endif

#define TEST_TARGET Input

#define vri (vr::VRInput())
#define VR_ASSERT_ENONE(func) VR_ASSERT_EQ(vr::VRInputError_None, func)

TEST(SetActionManifestPath) {
	char buf[1024];
	std::string path = std::string(VRTEST_ASSET_DIR) + "/actions.json";
	VR_ASSERT_EQ(0, vri->SetActionManifestPath(path.c_str()));
}
TEST(GetActionSetHandle) {
	// This should definitely succeed, do it first in case the more edge-pushing case below fails
	VR_ASSERT_EQ(vr::VRInputError_None, vri->GetActionSetHandle("/actions/main", &m_mainSet));
	VR_ASSERT_EQ(true, m_mainSet != vr::k_ulInvalidActionSetHandle);
	VR_ASSERT_EQ(vr::VRInputError_None, vri->GetActionSetHandle("/actions/single_sided", &m_singleSidedSet));
	VR_ASSERT_EQ(true, m_singleSidedSet != vr::k_ulInvalidActionSetHandle);

	// It seems you can pass in any old string and it will assign it an ID
	// By taking multiple different strings (thus ash3) we can check it's not passing back some
	// kind of dummy null value.
	vr::VRActionSetHandle_t ash1 = 1, ash2 = 2, ash3 = 3;
	VR_ASSERT_EQ(vr::VRInputError_None, vri->GetActionSetHandle("blahblahblah", &ash1));
	VR_ASSERT_EQ(vr::VRInputError_None, vri->GetActionSetHandle("blahblahblah", &ash2));
	VR_ASSERT_EQ(vr::VRInputError_None, vri->GetActionSetHandle("blahblahblah2", &ash3));
	VR_ASSERT_EQ(true, ash1 != 1);
	VR_ASSERT_EQ(true, ash2 != 2);
	VR_ASSERT_EQ(true, ash3 != 3);
	VR_ASSERT_EQ(ash1, ash2);
	VR_ASSERT_EQ(true, ash1 != ash3);
}
TEST(GetActionHandle) {
	// Do this first since it's more likely to succeed than the more pushy tests below, and we need it's value later
	VR_ASSERT_EQ(vr::VRInputError_None, vri->GetActionHandle("/actions/main/in/OpenInventory", &m_btn1));
	VR_ASSERT_EQ(true, m_btn1 != vr::k_ulInvalidActionSetHandle);

	VR_ASSERT_EQ(vr::VRInputError_None, vri->GetActionHandle("/actions/single_sided/in/hand_pose", &m_dualHandPose));
	VR_ASSERT_EQ(true, m_dualHandPose != vr::k_ulInvalidActionSetHandle);

	VR_ASSERT_EQ(vr::VRInputError_None, vri->GetActionHandle("/actions/main/in/LeftHand_Anim", &m_skel));
	VR_ASSERT_EQ(true, m_skel != vr::k_ulInvalidActionSetHandle);

	vr::VRActionHandle_t ah1 = 1, ah2 = 2, ah3 = 3;
	VR_ASSERT_EQ(vr::VRInputError_None, vri->GetActionHandle("testing", &ah1));
	VR_ASSERT_EQ(vr::VRInputError_None, vri->GetActionHandle("testing", &ah2));
	VR_ASSERT_EQ(vr::VRInputError_None, vri->GetActionHandle("testing2", &ah3));
	VR_ASSERT_EQ(true, ah1 != 1);
	VR_ASSERT_EQ(true, ah2 != 2);
	VR_ASSERT_EQ(true, ah3 != 3);
	VR_ASSERT_EQ(ah1, ah2);
	VR_ASSERT_EQ(true, ah1 != ah3);
}
TEST(GetInputSourceHandle) {
	VR_ASSERT_ENONE(vri->GetInputSourceHandle("/user/hand/left", &m_leftHandSource));
	VR_ASSERT(m_leftHandSource != vr::k_ulInvalidActionSetHandle);
	VR_ASSERT_ENONE(vri->GetInputSourceHandle("/user/hand/right", &m_rightHandSource));
	VR_ASSERT(m_rightHandSource != vr::k_ulInvalidActionSetHandle);
	VR_ASSERT(m_rightHandSource != m_leftHandSource);
}
TEST_RUN_GROUP(PerFrameAlways)
TEST(UpdateActionState) {
	const int numSets = 2;
	vr::VRActiveActionSet_t sets[numSets] = {};
	sets[0].ulActionSet = m_mainSet;
	sets[1].ulActionSet = m_singleSidedSet;
	VR_ASSERT_EQ(vr::VRInputError_None, vri->UpdateActionState(sets, sizeof(sets[0]), numSets));
}
TEST_RUN_GROUP(PerFrame)
TEST(GetDigitalActionData) {
	if (!remoteControlState)
		return;

	vr::InputDigitalActionData_t data;
	VR_ASSERT_EQ(vr::VRInputError_None,
	             vri->GetDigitalActionData(m_btn1, &data, sizeof(data), vr::k_ulInvalidInputValueHandle));

	// Set the 'first' flag here, in case we return early in an assert
	bool isFirstUpdate = m_firstDigitalUpdate;
	m_firstDigitalUpdate = false;

	// Everything is generally unpredictable on the first update, so just ignore it
	// Note we have to return after calling GetDigitalActionData, since stuff like bChanged is based on that
	if (isFirstUpdate)
		return;

	VR_ASSERT_EQ(true, data.bActive);
	VR_ASSERT_EQ(remoteControlState->left.btn_a, data.bState);

	// Test bChanged
	bool changed = remoteControlState->left.btn_a != lastRemoteControlState->left.btn_a;
	VR_ASSERT_EQ(changed, data.bChanged);

	// TODO fUpdateTime

	// Note this needs /click on the end. Just noting this since it took forever to find, and GetOriginLocalizedName
	// doesn't distinguish between that and when it's absent, which led me on a wild goose chase into the guts of
	// SteamVR and took nine months to find!
	vr::VRInputValueHandle_t src = 0, src2 = 1;
	VR_ASSERT_EQ(vr::VRInputError_None, vri->GetInputSourceHandle("/user/hand/left/input/a/click", &src));
	VR_ASSERT_EQ(vr::VRInputError_None, vri->GetInputSourceHandle("/user/hand/left/input/a/click", &src2));
	VR_ASSERT_EQ(src, src2);

	// Use this for debugging (really handy if tests fail, only works for steamvr obviously but easy to get the same
	// thing under OC)
	//
	// std::string lookedUp = lookup_handle_name(data.activeOrigin);
	// printf("Looked up name: %s\n", lookedUp.c_str());

	// TODO test ulRestrictToDevice

	VR_ASSERT_EQ(src, data.activeOrigin);
}
TODO_TEST(GetAnalogActionData)
TODO_TEST(GetPoseActionDataRelativeToNow)
TEST_RUN_GROUP(PerFrame)
TEST(GetPoseActionDataForNextFrame) {
	VR_ASSERT(m_dualHandPose != vr::k_ulInvalidActionHandle);

	vr::InputPoseActionData_t actionData = {};

	// Try reading both hands through a single action, using ulRestrictToDevice
	// In the input manifest, this pose is bound to multiple input sources

	// TODO test active origins by converting them to a string and checking them

	// Left hand
	VR_ASSERT_ENONE(vri->GetPoseActionDataForNextFrame(m_dualHandPose, vr::TrackingUniverseStanding, &actionData,
	                                                   sizeof(actionData), m_leftHandSource));
	VR_ASSERT_EQ(true, actionData.bActive);
	VR_ASSERT_EQ_FLOAT(-0.5f, actionData.pose.mDeviceToAbsoluteTracking.m[0][3]); // X coordinate

	// Right hand
	VR_ASSERT_ENONE(vri->GetPoseActionDataForNextFrame(m_dualHandPose, vr::TrackingUniverseStanding, &actionData,
	                                                   sizeof(actionData), m_rightHandSource));
	// We toggle this controller on and off with the remote. Unfortunately, Monado's SteamVR driver doesn't
	// actually disconnect a controller when we deactivate it with the remote control system, so it's marked
	// as always active.
	// VR_ASSERT_EQ(remoteControlState->right.active, actionData.bActive);
	// Also, skip checks on the activate/deactivate boundary to avoid out-by-one-frame errors which could easily come
	// from latency related to the Monado-remote driver in SteamVR.
	if (actionData.bActive && remoteControlState->right.active == lastRemoteControlState->right.active) {
		// The position goes to zero when the device is deactivated
		bool active = remoteControlState->right.active;
		VR_ASSERT_EQ_FLOAT(active ? 0.15f : 0.0f, actionData.pose.mDeviceToAbsoluteTracking.m[0][3]); // X coordinate
	}
}
TEST_RUN_GROUP(PerFrame)
TEST(GetSkeletalActionData) {
	VR_ASSERT(m_skel != vr::k_ulInvalidActionHandle);

	vr::InputSkeletalActionData_t actionData = {};
	VR_ASSERT_EQ(vr::VRInputError_None, vri->GetSkeletalActionData(m_skel, &actionData, sizeof(actionData)));

	if (actionData.bActive) {
		vr::VRBoneTransform_t bones[31];
		VR_ASSERT_EQ(vr::VRInputError_None, vri->GetSkeletalBoneData(m_skel, vr::VRSkeletalTransformSpace_Parent,
		                                                             vr::VRSkeletalMotionRange_WithController, bones,
		                                                             sizeof(bones) / sizeof(bones[0])));

		for (int i = 0; i < 31; i++) {
			const vr::VRBoneTransform_t &b = bones[i];
			const vr::HmdQuaternionf_t &q = b.orientation;
			const vr::HmdVector4_t &p = b.position;
			// printf("  bone %2d: %f,%f,%f for %f,%f,%f,%f\n", i, p.v[0], p.v[1], p.v[2], q.x, q.y, q.z, q.w);
		}
	}
}
TODO_TEST(GetDominantHand)
TODO_TEST(SetDominantHand)
TODO_TEST(GetBoneCount)
TODO_TEST(GetBoneHierarchy)
TODO_TEST(GetBoneName)
TODO_TEST(GetSkeletalReferenceTransforms)
TODO_TEST(GetSkeletalTrackingLevel)
TODO_TEST(GetSkeletalBoneData)
TODO_TEST(GetSkeletalSummaryData)
TODO_TEST(GetSkeletalBoneDataCompressed)
TODO_TEST(DecompressSkeletalBoneData)
TODO_TEST(TriggerHapticVibrationAction)
TODO_TEST(GetActionOrigins)
TODO_TEST(GetOriginLocalizedName)
TODO_TEST(GetOriginTrackedDeviceInfo)
TODO_TEST(GetActionBindingInfo)
TODO_TEST(ShowActionOrigins)
TODO_TEST(ShowBindingsForActionSet)
TODO_TEST(GetComponentStateForBinding)
TODO_TEST(IsUsingLegacyInput)
TODO_TEST(OpenBindingUI)
TODO_TEST(GetBindingVariant)
