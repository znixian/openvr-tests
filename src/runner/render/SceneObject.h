//
// Created by znix on 23/06/2021.
//

#pragma once

#include <memory>

#include "Model.h"

class SceneObject {
  public:
	explicit SceneObject(int shader);

	int shader = 0;
	std::shared_ptr<IModel> m_model;
	glm::mat4 transform = glm::identity<glm::mat4>();

	/**
	 * Draw this object to the screen.
	 *
	 * @param projection The inverse camera position and the projection matrices combined.
	 */
	void draw(glm::mat4 projection);

    void draw_at(glm::mat4 projection, glm::mat4 customTransform);

  protected:
	UniformLoc m_shaderVertMult = 0;
	UniformLoc m_objToWorld = 0;
};
