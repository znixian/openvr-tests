//
// Created by ZNix on 13/06/2021.
//

#pragma once

// Signature guessed from usages in OpenVR library
// NOLINTNEXTLINE(readability-identifier-naming)
void AssertMsg(bool valid, const char *msg);
