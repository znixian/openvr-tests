Note that headers not in the 'include' directory are only for use by the implementation, and are
not publicly exposed.

Source and revision:
https://github.com/syoyo/tinygltf/tree/64452bb5fa1779d5a65cc5fe8e60d77b0724918b
