// Copied from monado remote driver r_hub.c
// Copyright 2020, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0

#include "global.h"

#include "monado_remote.h"

#ifndef _WIN32

// Include the current version of the remote def file if we're in an IDE, or CMake hasn't set it
// Normally this file would be injected in with -include at build time.
// This allows the user to set a different version of the header, to make bisecting easier.
#if defined(__clang_analyzer__) || defined(__INTELLISENSE__) || defined(__JETBRAINS_IDE__) ||                          \
    !defined(MONADO_DEFS_REMOTE_PATH)
#include "monado_remote_def.h"
#endif

#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

// FIXME do this properly
#define U_LOG_E(...) fprintf(stderr, __VA_ARGS__)

static xrt_vec3 vec3_to_monado(glm::vec3 v) { return xrt_vec3{v.x, v.y, v.z}; }
static xrt_quat quat_to_monado(glm::quat q) { return xrt_quat{q.x, q.y, q.z, q.w}; }
xrt_pose MonadoConnection::GlmPose::to_monado() const {
	return xrt_pose{quat_to_monado(rotation), vec3_to_monado(position)};
}

void MonadoConnection::GlmPose::apply_inputs(r_remote_controller_data &data) const {
	data.active = active;
	data.a_click = btn_a;
	data.b_click = btn_b;
}

int MonadoConnection::connect_to_monado(IMonadoConnection **out, const char *ip_addr, uint16_t port) {
	*out = nullptr;

	struct sockaddr_in addr = {0};
	int ret;

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);

	ret = inet_pton(AF_INET, ip_addr, &addr.sin_addr);
	if (ret < 0) {
		U_LOG_E("socket failed: %i", ret);
		return ret;
	}

	ret = socket(AF_INET, SOCK_STREAM, 0);
	if (ret < 0) {
		U_LOG_E("socket failed: %i", ret);
		return ret;
	}

	int fd = ret;

	ret = connect(fd, (struct sockaddr *)&addr, sizeof(addr));
	if (ret < 0) {
		U_LOG_E("connect failed: %i", ret);
		close(fd);
		return ret;
	}

	int flags = 1;
	ret = setsockopt(fd, SOL_TCP, TCP_NODELAY, (void *)&flags, sizeof(flags));
	if (ret < 0) {
		U_LOG_E("setsockopt failed: %i", ret);
		close(fd);
		return ret;
	}

	MonadoConnection *conn = new MonadoConnection();
	*out = conn;
	conn->m_conn_fd = fd;

	struct r_remote_data data;
	conn->read_data(&data);
	conn->read_data(&data);

	return 0;
}

MonadoConnection::MonadoConnection() {
	for (int hand = 0; hand < 2; hand++) {
		for (int i = 0; i < 5; i++)
			m_finger_curls[hand][i] = 0.2;
	}
}

MonadoConnection::~MonadoConnection() {
	// TODO close everything
}

int MonadoConnection::read_data(struct r_remote_data *data) const {
	const size_t size = sizeof(*data);
	size_t current = 0;

	while (current < size) {
		void *ptr = (uint8_t *)data + current;

		ssize_t ret = read(m_conn_fd, ptr, size - current);
		if (ret < 0) {
			return ret;
		}

		if (ret > 0) {
			current += (size_t)ret;
		} else {
			abort(); // FIXME
			// U_LOG_I("Disconnected!");
			return -1;
		}
	}

	return 0;
}

int MonadoConnection::write_data(const struct r_remote_data *data) const {
	const size_t size = sizeof(*data);
	size_t current = 0;

	while (current < size) {
		const void *ptr = (const uint8_t *)data + current;

		ssize_t ret = write(m_conn_fd, ptr, size - current);
		if (ret < 0) {
			return ret;
		}

		if (ret > 0) {
			current += (size_t)ret;
		} else {
			abort(); // FIXME
			// U_LOG_I("Disconnected!");
			return -1;
		}
	}

	return 0;
}

void MonadoConnection::set_hmd_pose(glm::vec3 pos, glm::quat rot) { m_hmd_pose = GlmPose{pos, rot}; }

void MonadoConnection::set_hand_pose(glm::vec3 pos, glm::quat rot, vr::EVREye eye) {
	m_hand_poses[eye].position = pos;
	m_hand_poses[eye].rotation = rot;
}

void MonadoConnection::send_update() {
	struct r_remote_data data;
	memset(&data, 0, sizeof(data));

	data.header = R_HEADER_VALUE; // Not used, but leave it there for future-proofing
	data.hmd.pose = m_hmd_pose.to_monado();

	data.left.pose = m_hand_poses[vr::Eye_Left].to_monado();
	data.right.pose = m_hand_poses[vr::Eye_Right].to_monado();

	m_hand_poses[vr::Eye_Left].apply_inputs(data.left);
	m_hand_poses[vr::Eye_Right].apply_inputs(data.right);

	for (int i = 0; i < 5; i++) {
		data.left.hand_curl[i] = m_finger_curls[0][i];
		data.right.hand_curl[i] = m_finger_curls[1][i];
	}
	data.left.hand_tracking_active = true;
	data.right.hand_tracking_active = true;

	int result = write_data(&data);
	if (result) {
		fprintf(stderr, "Failed to write HMD update data: %d\n", result);
		abort();
	}
}

void MonadoConnection::set_control_state(vr::EVREye side, bool active, bool btn_a, bool btn_b) {
	GlmPose &pose = m_hand_poses[side];
	pose.active = active;
	pose.btn_a = btn_a;
	pose.btn_b = btn_b;
}

#endif

// Be careful that this is outside the big ifdef.
IMonadoConnection::~IMonadoConnection() = default;
